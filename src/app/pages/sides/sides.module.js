(function() {
    'use strict';
    angular.module('BlurAdmin.sides', [])
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('dashboard', {
                    url: '/dashboard',
                    templateUrl: 'app/pages/sides/sides.html'
                }).state('dashboard.home', {
                    url: '/home',
                    templateUrl: 'app/pages/home/dashboard.html',
                    title: 'Dashboard',
                    sidebarMeta: {
                      icon: 'ion-android-home',
                      order: 0,
                    },
                });
        }]);
})();
