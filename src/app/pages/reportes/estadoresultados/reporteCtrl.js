(function(){
    'use strict';
    angular.module('BlurAdmin.pages.reporte',[])
        .directive('validRangeEnd', [function(){
            return {
                require: 'ngModel',
                restrict: 'A',
                scope: {'dateInit':'='},
                link:function(scope, element, attr, ngModel){
                    ngModel.$validators.validRangeEnd=function(newValue, oldValue){
                        if(ngModel.$isEmpty(newValue) || !moment(newValue, "DD/MM/YYYY")._isValid){ return false;}
                        var fin=moment(newValue);
                        var inicio=moment(scope.dateInit, "DD/MM/YYYY");
                        return ((fin.diff(inicio, 'day')>=0));
                    };
                }
            }
        }])
        .controller('reporteviewCtrl', ['$scope', '$stateParams', 'serviceAPI', 'toastr', function($scope, $stateParams, serviceAPI, toastr){
            $scope.data={monto_vtacont:0.00, monto_vtacred:0.00, tot_vta:0.00, monto_ii:0.00, monto_if:0.00, monto_gv:0.00, tot_cv:0.00, utilbruta_er:0.00, monto_gg:0.00, utiloperativ_er:0.00, impuestos_er:0.00};
            $scope.consultar=function(){
                if($stateParams.id_er=='' || $stateParams.id_area=='' || angular.isUndefined($stateParams.id_er) || angular.isUndefined($stateParams.id_area))
                    return false;
                serviceAPI.getResultPeriodo({id: $stateParams.id_er, id_area:$stateParams.id_area}).success(function(r){
                    if(r.success && r.num_rows==1){
                        angular.extend($scope.data, r.rows);
                    }
                });
            };

        }])
        .controller('reporteCtrl', [ '$scope', '$state', '$stateParams', 'serviceAPI', 'toastr', function($scope, $state, $stateParams, serviceAPI, toastr){
            $scope.default={id_area:'', inicio:'', fin:''};
            $scope.data={id_area:'', inicio:'', fin:''};
            $scope.areas_proyectos=[];
            var areas_proyectos=[];
            serviceAPI.getAreaproyectos().success(function(r){
                if(r.success && r.rows.length>0){
                    areas_proyectos=r.rows.map(function(i){ return {id: i.id_arp ,label: i.desc_arp};});
                    angular.copy(areas_proyectos, $scope.areas_proyectos);
                }
            });
            $scope.consultar=function(){
                serviceAPI.existResultPeriodo($scope.data).success(function(r){
                    if(r.success && r.num_rows>0){
                        $state.go('dashboard.reportes.estado_resultados-details', {id_er:r.rows, id_area: $scope.data.id_area.id});
                    }
                    toastr.warning(r.messages, 'SOFTPG');
                });
            };
        }])
        
})();