<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
$data = json_decode(file_get_contents('php://input'));
$response_json	=array('success'=>false, 'auth'=>false, 'admin'=>false, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
if(isset($data->username, $data->clave) && (!empty($data->username) && !empty($data->clave) )){
	require_once("./class/GLibfunciones.php");
    $OConex = new GConector();
    $init_stmt=$OConex->stmt_init();
    $oLogin = new GLogin();
    $data->clave=md5(sha1($data->clave));
    $sql=$oLogin->signin();
    if(!$init_stmt->prepare($sql))
        throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
    if(!$init_stmt->bind_param('ss', $data->username, $data->clave))
        throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
    $init_stmt->execute();
    $result=$init_stmt->get_result();
    $response_json['success']=true;
    $response_json['auth']=($result->num_rows==1);
    if($response_json['auth']){
        $response_json['rows']=$result->fetch_assoc();
        $response_json['rows']['states']=array();
    }
    $response_json['messages']=(!$response_json['auth'])?"Los datos de acceso no coinciden en el sistema":"";
}
echo json_encode($response_json);
?>
