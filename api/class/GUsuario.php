<?php
class GUsuario {
    public function __construct(){

    }

    public function updateStatusUsuario(){
        return "UPDATE usuario SET status=? WHERE id_usr=?";
    }

    public function getUsuario(){
        return "SELECT * FROM usuario WHERE id_usr=?";
    }

    public function actualizar(){
        return "UPDATE usuario SET nomb_usr=?, clave_usr=?, rol_usr=? WHERE id_usr=?";
    }
    public function agregar(){
        return "INSERT INTO usuario (nomb_usr, clave_usr, rol_usr, id_usr) VALUES (?, ?, ?, ?)";
    }

    public function listar(){
        return "SELECT id_usr, nomb_usr, ELT(rol_usr, 'Administrador', 'Gerente', 'Asistente') AS nom_rol FROM usuario ORDER BY nomb_usr";
    }

    public function exitsUsuario(){
        return "SELECT id_usr FROM usuario WHERE nomb_usr=? AND id_usr!=?";
    }
}
?>