<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
    $a_oper=array("listar", "guardar", "consultar", "statuschange", "existareaproye", "eliminar");
    if(!isset($_GET['oper']) || !in_array($_GET['oper'], $a_oper)){
        die();
    }
    $response_json	=array('success'=>false, 'auth'=>false, 'admin'=>false, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
    $data = json_decode(file_get_contents('php://input'));
    include './class/GLibfunciones.php';
    $OConex=new GConector();
    $init_stmt=$OConex->stmt_init();
    $oAreaproy=new GAreaproyecto();
    switch($_GET['oper']){
        case 'eliminar':
            if(!isset($data->id) || empty($data->id))
                break;
            $sql=$oAreaproy->eliminar();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('i', $data->id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $response_json['success']=true;
            $response_json['affected_rows']=$init_stmt->affected_rows;
            if($response_json['affected_rows']!=1)
                $response_json['messages']="No se pudo eliminar la información";
        break;
        case 'consultar':
            if(!isset($data->id) || empty($data->id))
                break;
            $sql=$oAreaproy->consultar();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('i', $data->id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $result_stmt=$init_stmt->get_result();
            $response_json['success']=true;
            $response_json['num_rows']=$result_stmt->num_rows;
            if($result_stmt->num_rows==1)
                $response_json['rows']=array_combine(array('id_areaproy', 'nom_areaproy'), array_values($result_stmt->fetch_assoc()));
            else
                $response_json['messages']="No se encontraron registros que coincida con la consulta";
        break;
        case 'guardar':
            if(!isset($data->nom_areaproy, $data->id_areaproy) || empty($data->nom_areaproy))
                break;
            $sql=call_user_func(array($oAreaproy, (empty($data->id_areaproy))?'agregar':'actualizar'));
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('si', $data->nom_areaproy, $data->id_areaproy))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $response_json['success']=true;
            $response_json['affected_rows']=$init_stmt->affected_rows;
            $response_json['rows']['id']=(empty($data->id_areaproy))?$init_stmt->insert_id:$data->id_areaproy;
            if($init_stmt->affected_rows!=1)
                $response_json['messages']=(empty($data->id_areaproy))?"No se pudo registrar los datos":"No ocurrio cambios en el registro";
            else
                $response_json['messages']="Los datos fueron registrados satisfactoriamente";
        break;
        case 'existareaproye':
            if(!isset($data->descripcion, $data->id) || empty($data->descripcion))
                break;
            $sql=$oAreaproy->existsDescripcion();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('si', $data->descripcion, $data->id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $result_stmt=$init_stmt->get_result();
            $response_json['success']=true;
            $response_json['num_rows']=$result_stmt->num_rows;
        break;
        case 'listar':
            $store_params=array(0=>'');
            if(isset($data->predicateObject)){
                foreach($data->predicateObject as $fields => $value){
                    $store_params[0].='s';
                    ${$fields}=sprintf("%%%s%%",$value);
                    $store_params[]=&${$fields};
                    $oAreaproy->addFilter($fields);
                }
            }
            $sql=$oAreaproy->listar();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(count($store_params)>1){
                if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
                    throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
            }
            $init_stmt->execute();
            $result_rows=$init_stmt->get_result();
            $response_json['totalItemCount']=$result_rows->num_rows;
            $response_json['success']=true;
            if($result_rows->num_rows==0)
                break;
            if(isset($data->start, $data->number)){
                $response_json['numberOfPages']=ceil($result_rows->num_rows/$data->number);
                $Opagination=new GPagination();
                $Opagination->setInit($data->start);
                $Opagination->setLimit($data->number);
                $sql=$Opagination->prepareSQL($sql);
            }
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(count($store_params)>1){
                if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
                    throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
            }
            $init_stmt->execute();
            $result=$init_stmt->get_result();
            $i=(isset($data->start))?$data->start:0;
            $number=(isset($data->number))?$data->number:10;
            while($row=$result->fetch_assoc()){
                array_push($response_json['rows'], array_merge($row, array('item'=>++$i)));
            }
            $response_json['totalItemCount']=count($response_json['rows']);
        break;
    }
    echo json_encode($response_json);
}catch(Exception $e){
    echo $e->getOutMsg();
}
?>