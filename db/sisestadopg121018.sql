-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-10-2018 a las 01:59:44
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.0.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sisestadopg`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `generate_estado_resultado` (IN `periodo` DATE, IN `id_area` INT)  BEGIN
	DECLARE done, exist_periodo_area, lc_mes, lc_anio INT DEFAULT 0; 
    DECLARE lc_periodo VARCHAR(7) DEFAULT NULL;
    DECLARE lc_id_er, lc_id_vta, lc_id_gg, lc_id_cv INT DEFAULT NULL;
    DECLARE lc_impuesto, lc_monto_vta, lc_monto_gg, lc_monto_cv, lc_saldoinicial_er, lc_utilbruta_er, lc_utiloperativ_er, lc_impuesto_er, lc_utilneta_er DOUBLE(13,2) DEFAULT 0.00;
    DECLARE curs1 CURSOR FOR SELECT id_er, utilneta_er, DATE_FORMAT(periodo_er, '%c') AS mes, DATE_FORMAT(periodo_er, '%Y') AS anio, periodo_er, IFNULL(saldoinicial_er,0.00) AS saldoinicial_er FROM estado_resultado WHERE id_arp=id_area AND DATE_FORMAT(periodo_er, '%Y')<=DATE_FORMAT(periodo,'%Y') ORDER BY 4, 3 DESC LIMIT 0,2;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    /*OBTENER VALOR DE LAS TABLAS ventas, gastos_generales, costo_ventas*/
    SELECT factor_imp INTO lc_impuesto FROM impuestos WHERE activo_imp=1;/*factor_impusto*/
    SELECT id_vta, monto_vta INTO lc_id_vta, lc_monto_vta FROM ventas WHERE id_arp=id_area AND DATE_FORMAT(periodo_vta, '%m-%Y')=DATE_FORMAT(periodo, '%m-%Y');
    SELECT id_gg, monto_gg INTO lc_id_gg, lc_monto_gg FROM gastos_generales WHERE id_arp=id_area AND DATE_FORMAT(periodo_gg, '%m-%Y')=DATE_FORMAT(periodo, '%m-%Y');
    SELECT id_cv, monto_cv INTO lc_id_cv, lc_monto_cv FROM costo_ventas WHERE id_arp=id_area AND DATE_FORMAT(periodo_cv, '%m-%Y')=DATE_FORMAT(periodo, '%m-%Y');
    OPEN curs1;
    SELECT FOUND_ROWS() INTO exist_periodo_area;
    SET done:=IF(exist_periodo_area=0,1,0);
	IF exist_periodo_area=0 THEN
   		SET lc_saldoinicial_er=0;
        SET lc_periodo=periodo;
        PREPARE stmt FROM 'INSERT INTO estado_resultado (saldoinicial_er, utilbruta_er, id_vta, monto_vta, id_cv, monto_cv, id_gg, monto_gg, utiloperativ_er, impuestos_er, utilneta_er, periodo_er, id_arp, id_er) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        /*
		INSERT INTO estado_resultado (saldoinicial_er, id_vta, monto_vta, id_cv, monto_cv, id_gg, monto_gg, utiloperativ_er, impuestos_er, utilneta_er, periodo_er, id_arp) VALUES (lc_saldoinicial_er, lc_id_vta, lc_monto_vta, lc_id_cv, lc_monto_cv, lc_id_gg, lc_monto_gg, lc_utiloperativ_er, lc_impuesto_er, lc_utilneta_er, periodo, id_area);
        */
	ELSE 
		read_loop: LOOP
			FETCH curs1 INTO lc_id_er, lc_utilneta_er, lc_mes, lc_anio, lc_periodo, lc_saldoinicial_er;
            INSERT INTO test_trigger (periodo_1, lc_done) VALUE (DATE_FORMAT(lc_periodo, '%m-%Y'), done);
			IF done = 1 THEN
				LEAVE read_loop;
			END IF;
			IF DATE_FORMAT(lc_periodo, '%m-%Y')=DATE_FORMAT(periodo, '%m-%Y') THEN
            	PREPARE stmt FROM 'UPDATE estado_resultado SET saldoinicial_er=?, utilbruta_er=?, id_vta=?, monto_vta=?, id_cv=?, monto_cv=?, id_gg=?, monto_gg=?, utiloperativ_er=?, impuestos_er=?, utilneta_er=?, periodo_er=?, id_arp=? WHERE id_er=?';
                LEAVE read_loop;
               	/*UPDATE estado_resultado SET WHERE id_er=lc_id_er;*/
            ELSE
            	SET lc_saldoinicial_er:=lc_utilneta_er*0.50;
                SET lc_periodo:=periodo;
                SET lc_id_er=NULL;
		        PREPARE stmt FROM 'INSERT INTO estado_resultado (saldoinicial_er, utilbruta_er, id_vta, monto_vta, id_cv, monto_cv, id_gg, monto_gg, utiloperativ_er, impuestos_er, utilneta_er, periodo_er, id_arp, id_er) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
                LEAVE read_loop;
            END IF;
		END LOOP;
	END IF;    
    CLOSE curs1;
    SET lc_utilbruta_er:=lc_saldoinicial_er+lc_monto_vta-lc_monto_cv;
    SET lc_utiloperativ_er:=lc_utilbruta_er-lc_monto_gg;
    SET lc_impuesto_er:=lc_utiloperativ_er*lc_impuesto;
    SET lc_utilneta_er:=lc_utiloperativ_er-lc_impuesto_er;
    EXECUTE stmt USING @lc_saldoinicial_er, @lc_utilbruta_er, @lc_id_vta, @lc_monto_vta, @lc_id_cv, @lc_monto_cv, @lc_id_gg, @lc_monto_gg, @lc_utiloperativ_er, @lc_impuesto_er, @lc_utilneta_er, @lc_periodo, @id_area, @lc_id_er;
    DEALLOCATE PREPARE stmt;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `registro_costo_ventas` (IN `periodo` DATE, IN `id_area` INT)  BEGIN
	DECLARE lc_id_cv INT DEFAULT NULL;
    DECLARE exist_row INT DEFAULT 0;
    DECLARE lc_tot_cv, lc_monto_ii, lc_monto_if, lc_monto_gv DOUBLE(13,2) DEFAULT 0.00;
    DECLARE curs1 CURSOR FOR SELECT id_cv FROM costo_ventas WHERE id_arp=id_area AND DATE_FORMAT(periodo_cv, '%m-%Y')=DATE_FORMAT( periodo, '%m-%Y');
    SELECT monto_ii_inv, monto_if_inv INTO lc_monto_ii, lc_monto_if FROM inventarios WHERE id_arp=id_area AND DATE_FORMAT(periodo_ii_inv, '%m-%Y')=DATE_FORMAT(periodo,'%m-%Y');
	SELECT monto_gv INTO lc_monto_gv FROM gasto_ventas WHERE id_arp=id_area AND DATE_FORMAT(periodo_gv, '%m-%Y')=DATE_FORMAT(periodo,'%m-%Y');    
    SET lc_tot_cv=lc_monto_ii+lc_monto_gv-lc_monto_if;
    OPEN curs1;
    SELECT FOUND_ROWS() INTO exist_row;
    IF exist_row=1 THEN 
    	FETCH curs1 INTO lc_id_cv;
    	UPDATE costo_ventas SET monto_cv=lc_tot_cv WHERE id_cv=lc_id_cv;
    ELSE
    	INSERT INTO costo_ventas (periodo_cv, id_arp, monto_cv) VALUES (periodo, id_area, lc_tot_cv);
    END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `registro_gastos_generales` (IN `periodo` DATE, IN `id_area` INT)  BEGIN
	DECLARE lc_id_gg, lc_id_sue, lc_id_inu, lc_id_srv, lc_id_alq, lc_id_otri, lc_id_otrg INT DEFAULT NULL;
    DECLARE exist_row INT DEFAULT 0;
    DECLARE lc_tot_gg, lc_monto_sue, lc_monto_inu, lc_monto_srv, lc_monto_alq, lc_monto_otri, lc_monto_otrg DOUBLE(13,2) DEFAULT 0.00;
    DECLARE curs1 CURSOR FOR SELECT id_gg FROM gastos_generales WHERE id_arp=id_area AND DATE_FORMAT(periodo_gg, '%m-%Y')=DATE_FORMAT( periodo, '%m-%Y');
    SELECT id_sue, monto_sue INTO lc_id_sue, lc_monto_sue FROM sueldos WHERE id_arp=id_area AND DATE_FORMAT(periodo_sue, '%m-%Y')=DATE_FORMAT(periodo,'%m-%Y');
	SELECT id_inu, monto_inu INTO lc_id_inu, lc_monto_inu FROM insumos_utiles WHERE id_arp=id_area AND DATE_FORMAT(periodo_inu, '%m-%Y')=DATE_FORMAT(periodo,'%m-%Y');    
	SELECT id_srv, monto_srv INTO lc_id_srv, lc_monto_srv FROM servicios WHERE id_arp=id_area AND DATE_FORMAT(periodo_srv, '%m-%Y')=DATE_FORMAT(periodo,'%m-%Y');
	SELECT id_alq, monto_alq INTO lc_id_alq, lc_monto_alq FROM alquiler WHERE id_arp=id_area AND DATE_FORMAT(periodo_alq, '%m-%Y')=DATE_FORMAT(periodo,'%m-%Y');
	SELECT id_otri, monto_otri INTO lc_id_otri, lc_monto_otri FROM otros_impuestos WHERE id_arp=id_area AND DATE_FORMAT(periodo_otri, '%m-%Y')=DATE_FORMAT(periodo,'%m-%Y');
	SELECT id_otrg, monto_otrg INTO lc_id_otrg, lc_monto_otrg FROM otros_gastos WHERE id_arp=id_area AND DATE_FORMAT(periodo_otrg, '%m-%Y')=DATE_FORMAT(periodo,'%m-%Y');
    SET lc_tot_gg=lc_monto_sue+lc_monto_inu+lc_monto_srv+lc_monto_alq+lc_monto_otri+lc_monto_otrg;
    OPEN curs1;
    SELECT FOUND_ROWS() INTO exist_row;
    IF exist_row=1 THEN 
    	FETCH curs1 INTO lc_id_gg;
    	UPDATE gastos_generales SET id_sue=lc_id_sue, monto_sue=lc_monto_sue, id_inu=lc_id_inu, monto_inu=lc_monto_inu, id_srv=lc_id_srv, monto_srv=lc_monto_srv, id_alq=lc_id_alq, monto_alq=lc_monto_alq, id_otri=lc_id_otri, monto_otri=lc_monto_otri, id_otrg=lc_id_otrg, monto_otrg=lc_monto_otrg WHERE id_gg=lc_id_gg;
    ELSE
    	INSERT INTO gastos_generales (id_sue, monto_sue, id_inu, monto_inu, id_srv, monto_srv, id_alq, monto_alq, id_otri, monto_otri, id_otrg, monto_otrg, id_arp, periodo_gg) VALUES (lc_id_sue, lc_monto_sue, lc_id_inu, lc_monto_inu, lc_id_srv, lc_monto_srv, lc_id_alq, lc_monto_alq, lc_id_otri, lc_monto_otri, lc_id_otrg, lc_monto_otrg, id_area, periodo);
    END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `registro_ventas` (IN `periodo` DATE, IN `id_area` INT)  BEGIN
	DECLARE lc_id_vta, lc_id_vtacred, lc_id_vtacont INT DEFAULT NULL;
    DECLARE exist_row INT DEFAULT 0;
    DECLARE lc_tot_vta, lc_monto_vtacred, lc_monto_vtacont DOUBLE(13,2) DEFAULT 0.00;
    DECLARE curs1 CURSOR FOR SELECT id_vta FROM ventas WHERE id_arp=id_area AND DATE_FORMAT(periodo_vta, '%m-%Y')=DATE_FORMAT( periodo, '%m-%Y');
    SELECT monto_vtacred INTO lc_monto_vtacred FROM credito WHERE id_arp=id_area AND DATE_FORMAT(periodo_vtacred, '%m-%Y')=DATE_FORMAT(periodo,'%m-%Y');
	SELECT monto_vtacont INTO lc_monto_vtacont FROM contado WHERE id_arp=id_area AND DATE_FORMAT(periodo_vtacont, '%m-%Y')=DATE_FORMAT(periodo,'%m-%Y');    
    SET lc_tot_vta=lc_monto_vtacred+lc_monto_vtacont;
    OPEN curs1;
    SELECT FOUND_ROWS() INTO exist_row;
    IF exist_row=1 THEN 
    	FETCH curs1 INTO lc_id_vta;
    	UPDATE ventas SET monto_vta=lc_tot_vta WHERE id_vta=lc_id_vta;
    ELSE
    	INSERT INTO ventas (periodo_vta, monto_vta, id_arp, id_vta) VALUES (periodo, lc_tot_vta, id_area, NULL);
    END IF;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alquiler`
--

CREATE TABLE `alquiler` (
  `id_alq` int(11) NOT NULL,
  `periodo_alq` date DEFAULT NULL,
  `monto_alq` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `alquiler`
--

INSERT INTO `alquiler` (`id_alq`, `periodo_alq`, `monto_alq`, `id_arp`) VALUES
(1, '2017-01-31', '1500.00', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area_proyecto`
--

CREATE TABLE `area_proyecto` (
  `id_arp` int(11) NOT NULL,
  `desc_arp` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `area_proyecto`
--

INSERT INTO `area_proyecto` (`id_arp`, `desc_arp`) VALUES
(7, 'Proyecto A'),
(8, 'Proyecto B'),
(9, 'Proyecto C'),
(10, 'Proyecto D');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contado`
--

CREATE TABLE `contado` (
  `id_vtacont` int(11) NOT NULL,
  `periodo_vtacont` date DEFAULT NULL,
  `monto_vtacont` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `contado`
--

INSERT INTO `contado` (`id_vtacont`, `periodo_vtacont`, `monto_vtacont`, `id_arp`) VALUES
(1, '2017-01-01', '138450.00', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `costo_ventas`
--

CREATE TABLE `costo_ventas` (
  `id_cv` int(11) NOT NULL,
  `periodo_cv` date DEFAULT NULL,
  `monto_cv` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Disparadores `costo_ventas`
--
DELIMITER $$
CREATE TRIGGER `estado_resultado_cv` AFTER INSERT ON `costo_ventas` FOR EACH ROW CALL test_cursor(NEW.periodo_cv, NEW.id_arp)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `credito`
--

CREATE TABLE `credito` (
  `id_vtacred` int(11) NOT NULL,
  `periodo_vtacred` date DEFAULT NULL,
  `monto_vtacred` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `credito`
--

INSERT INTO `credito` (`id_vtacred`, `periodo_vtacred`, `monto_vtacred`, `id_arp`) VALUES
(1, '2017-01-01', '50000.00', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_resultado`
--

CREATE TABLE `estado_resultado` (
  `id_er` int(11) NOT NULL,
  `id_vta` int(11) DEFAULT NULL,
  `monto_vta` decimal(10,2) DEFAULT '0.00',
  `id_cv` int(11) DEFAULT NULL,
  `monto_cv` decimal(10,2) DEFAULT '0.00',
  `utilbruta_er` decimal(10,2) DEFAULT NULL,
  `id_gg` int(11) DEFAULT NULL,
  `monto_gg` decimal(10,2) DEFAULT '0.00',
  `utiloperativ_er` decimal(10,2) DEFAULT NULL,
  `impuestos_er` decimal(10,2) DEFAULT NULL,
  `utilneta_er` decimal(10,2) DEFAULT NULL,
  `periodo_er` date DEFAULT NULL,
  `id_arp` int(11) NOT NULL,
  `saldoinicial_er` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `estado_resultado`
--

INSERT INTO `estado_resultado` (`id_er`, `id_vta`, `monto_vta`, `id_cv`, `monto_cv`, `utilbruta_er`, `id_gg`, `monto_gg`, `utiloperativ_er`, `impuestos_er`, `utilneta_er`, `periodo_er`, `id_arp`, `saldoinicial_er`) VALUES
(1, 1, '50000.00', NULL, '0.00', NULL, 1, '0.00', '50001.00', '500010.00', '-450009.00', '2017-01-01', 7, '0.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos_generales`
--

CREATE TABLE `gastos_generales` (
  `id_gg` int(11) NOT NULL,
  `id_arp` int(11) NOT NULL,
  `id_sue` int(11) DEFAULT NULL,
  `monto_sue` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_inu` int(11) DEFAULT NULL,
  `monto_inu` decimal(10,2) DEFAULT '0.00',
  `id_srv` int(11) DEFAULT NULL,
  `monto_srv` decimal(10,2) DEFAULT '0.00',
  `id_alq` int(11) DEFAULT NULL,
  `monto_alq` decimal(10,2) DEFAULT '0.00',
  `id_otri` int(11) DEFAULT NULL,
  `monto_otri` decimal(10,2) DEFAULT '0.00',
  `id_otrg` int(11) DEFAULT NULL,
  `monto_otrg` decimal(10,2) DEFAULT '0.00',
  `periodo_gg` date DEFAULT NULL,
  `monto_gg` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `gastos_generales`
--

INSERT INTO `gastos_generales` (`id_gg`, `id_arp`, `id_sue`, `monto_sue`, `id_inu`, `monto_inu`, `id_srv`, `monto_srv`, `id_alq`, `monto_alq`, `id_otri`, `monto_otri`, `id_otrg`, `monto_otrg`, `periodo_gg`, `monto_gg`) VALUES
(1, 7, NULL, '0.00', 1, '458.00', NULL, '0.00', 1, '1500.00', NULL, '0.00', NULL, '0.00', '2017-01-31', NULL);

--
-- Disparadores `gastos_generales`
--
DELIMITER $$
CREATE TRIGGER `estado_resultado_gg` AFTER INSERT ON `gastos_generales` FOR EACH ROW CALL test_cursor(NEW.periodo_gg, NEW.id_arp)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `estado_resutado_gg_update` AFTER UPDATE ON `gastos_generales` FOR EACH ROW CALL test_cursor(OLD.periodo_gg, OLD.id_arp)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto_ventas`
--

CREATE TABLE `gasto_ventas` (
  `id_gv` int(11) NOT NULL,
  `periodo_gv` date DEFAULT NULL,
  `monto_gv` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuestos`
--

CREATE TABLE `impuestos` (
  `id_imp` int(11) NOT NULL,
  `factor_imp` decimal(10,2) DEFAULT NULL,
  `activo_imp` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `impuestos`
--

INSERT INTO `impuestos` (`id_imp`, `factor_imp`, `activo_imp`) VALUES
(1, '10.00', 1),
(5, '18.00', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insumos_utiles`
--

CREATE TABLE `insumos_utiles` (
  `id_inu` int(11) NOT NULL,
  `periodo_inu` date DEFAULT NULL,
  `monto_inu` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `insumos_utiles`
--

INSERT INTO `insumos_utiles` (`id_inu`, `periodo_inu`, `monto_inu`, `id_arp`) VALUES
(1, '2017-01-31', '458.00', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventarios`
--

CREATE TABLE `inventarios` (
  `id_inv` int(11) NOT NULL,
  `periodo_ii_inv` date DEFAULT NULL,
  `monto_ii_inv` decimal(10,2) DEFAULT NULL,
  `periodo_if_inv` date DEFAULT NULL,
  `monto_if_inv` decimal(10,2) DEFAULT '0.00',
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `otros_gastos`
--

CREATE TABLE `otros_gastos` (
  `id_otrg` int(11) NOT NULL,
  `periodo_otrg` date DEFAULT NULL,
  `monto_otrg` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `otros_gastos`
--

INSERT INTO `otros_gastos` (`id_otrg`, `periodo_otrg`, `monto_otrg`, `id_arp`) VALUES
(1, '2017-01-01', '568.00', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `otros_impuestos`
--

CREATE TABLE `otros_impuestos` (
  `id_otri` int(11) NOT NULL,
  `periodo_otri` date DEFAULT NULL,
  `monto_otri` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `otros_impuestos`
--

INSERT INTO `otros_impuestos` (`id_otri`, `periodo_otri`, `monto_otri`, `id_arp`) VALUES
(2, '2017-01-31', '564.32', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `id_srv` int(11) NOT NULL,
  `periodo_srv` date DEFAULT NULL,
  `monto_srv` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`id_srv`, `periodo_srv`, `monto_srv`, `id_arp`) VALUES
(1, '2017-01-31', '148.65', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sueldos`
--

CREATE TABLE `sueldos` (
  `id_sue` int(11) NOT NULL,
  `periodo_sue` date DEFAULT NULL,
  `monto_sue` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sueldos`
--

INSERT INTO `sueldos` (`id_sue`, `periodo_sue`, `monto_sue`, `id_arp`) VALUES
(1, '2017-01-31', '1860.00', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `test_estado_resultado`
--

CREATE TABLE `test_estado_resultado` (
  `mes` int(11) NOT NULL,
  `anio` int(11) NOT NULL,
  `periodo` varchar(7) COLLATE utf8_spanish2_ci NOT NULL,
  `monto_vta` double(13,2) NOT NULL,
  `monto_cv` double(13,2) NOT NULL,
  `monto_gg` double(13,2) NOT NULL,
  `saldoinicial_er` double(13,2) NOT NULL,
  `id_er` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `test_estado_resultado`
--

INSERT INTO `test_estado_resultado` (`mes`, `anio`, `periodo`, `monto_vta`, `monto_cv`, `monto_gg`, `saldoinicial_er`, `id_er`) VALUES
(0, 0, '01-2018', 0.00, 0.00, 0.00, 0.00, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `test_trigger`
--

CREATE TABLE `test_trigger` (
  `periodo_1` varchar(7) COLLATE utf8_spanish2_ci NOT NULL,
  `lc_done` tinyint(1) NOT NULL,
  `num_rows` int(11) NOT NULL,
  `utilbruta_er` decimal(13,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `test_trigger`
--

INSERT INTO `test_trigger` (`periodo_1`, `lc_done`, `num_rows`, `utilbruta_er`) VALUES
('2018-01', 0, 1, '0.00'),
('2018-01', 1, 1, '0.00'),
('2018-01', 0, 1, '0.00'),
('01-2018', 0, 1, '0.00'),
('2017-01', 0, 0, '0.00'),
('', 0, 0, '0.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usr` int(11) NOT NULL,
  `nomb_usr` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `clave_usr` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `rol_usr` enum('1','2','3') COLLATE utf8_unicode_ci NOT NULL DEFAULT '3',
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usr`, `nomb_usr`, `clave_usr`, `rol_usr`, `status`) VALUES
(1, 'usuario', '833693d43e103bda03c45024d6472dc1', '1', '1'),
(2, 'root', 'f64ba71a5d1ccd651a3d742b1e9c5c77', '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id_vta` int(11) NOT NULL,
  `periodo_vta` date DEFAULT NULL,
  `monto_vta` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id_vta`, `periodo_vta`, `monto_vta`, `id_arp`) VALUES
(1, '2017-01-01', '188450.00', 7);

--
-- Disparadores `ventas`
--
DELIMITER $$
CREATE TRIGGER `estado_resultado_ventas` AFTER INSERT ON `ventas` FOR EACH ROW CALL test_cursor(NEW.periodo_vta, NEW.id_arp)
$$
DELIMITER ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alquiler`
--
ALTER TABLE `alquiler`
  ADD PRIMARY KEY (`id_alq`);

--
-- Indices de la tabla `area_proyecto`
--
ALTER TABLE `area_proyecto`
  ADD PRIMARY KEY (`id_arp`);

--
-- Indices de la tabla `contado`
--
ALTER TABLE `contado`
  ADD PRIMARY KEY (`id_vtacont`);

--
-- Indices de la tabla `costo_ventas`
--
ALTER TABLE `costo_ventas`
  ADD PRIMARY KEY (`id_cv`);

--
-- Indices de la tabla `credito`
--
ALTER TABLE `credito`
  ADD PRIMARY KEY (`id_vtacred`),
  ADD KEY `id_arp_fk` (`id_arp`);

--
-- Indices de la tabla `estado_resultado`
--
ALTER TABLE `estado_resultado`
  ADD PRIMARY KEY (`id_er`);

--
-- Indices de la tabla `gastos_generales`
--
ALTER TABLE `gastos_generales`
  ADD PRIMARY KEY (`id_gg`);

--
-- Indices de la tabla `gasto_ventas`
--
ALTER TABLE `gasto_ventas`
  ADD PRIMARY KEY (`id_gv`);

--
-- Indices de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  ADD PRIMARY KEY (`id_imp`);

--
-- Indices de la tabla `insumos_utiles`
--
ALTER TABLE `insumos_utiles`
  ADD PRIMARY KEY (`id_inu`);

--
-- Indices de la tabla `inventarios`
--
ALTER TABLE `inventarios`
  ADD PRIMARY KEY (`id_inv`);

--
-- Indices de la tabla `otros_gastos`
--
ALTER TABLE `otros_gastos`
  ADD PRIMARY KEY (`id_otrg`);

--
-- Indices de la tabla `otros_impuestos`
--
ALTER TABLE `otros_impuestos`
  ADD PRIMARY KEY (`id_otri`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id_srv`);

--
-- Indices de la tabla `sueldos`
--
ALTER TABLE `sueldos`
  ADD PRIMARY KEY (`id_sue`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usr`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id_vta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alquiler`
--
ALTER TABLE `alquiler`
  MODIFY `id_alq` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `area_proyecto`
--
ALTER TABLE `area_proyecto`
  MODIFY `id_arp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `contado`
--
ALTER TABLE `contado`
  MODIFY `id_vtacont` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `costo_ventas`
--
ALTER TABLE `costo_ventas`
  MODIFY `id_cv` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `credito`
--
ALTER TABLE `credito`
  MODIFY `id_vtacred` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `estado_resultado`
--
ALTER TABLE `estado_resultado`
  MODIFY `id_er` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `gastos_generales`
--
ALTER TABLE `gastos_generales`
  MODIFY `id_gg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `gasto_ventas`
--
ALTER TABLE `gasto_ventas`
  MODIFY `id_gv` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  MODIFY `id_imp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `insumos_utiles`
--
ALTER TABLE `insumos_utiles`
  MODIFY `id_inu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `inventarios`
--
ALTER TABLE `inventarios`
  MODIFY `id_inv` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `otros_gastos`
--
ALTER TABLE `otros_gastos`
  MODIFY `id_otrg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `otros_impuestos`
--
ALTER TABLE `otros_impuestos`
  MODIFY `id_otri` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id_srv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sueldos`
--
ALTER TABLE `sueldos`
  MODIFY `id_sue` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id_vta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `credito`
--
ALTER TABLE `credito`
  ADD CONSTRAINT `id_arp_fk` FOREIGN KEY (`id_arp`) REFERENCES `area_proyecto` (`id_arp`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
