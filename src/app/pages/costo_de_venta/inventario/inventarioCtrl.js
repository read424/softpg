(function(){
    'use strict';
    angular.module('BlurAdmin.pages.costo_de_venta.inventario',[])
        .controller('inventarioCtrl', [ '$scope', 'serviceAPI', 'toastr', '$state', '$stateParams', '$timeout', function($scope, serviceAPI, toastr, $state, $stateParams, $timeout){
            $scope.data={id_inv:0, id_area:'', periodo:'', monto_ini:0.00, monto_fin:0.00};
            $scope.areas_proyectos=[];
            serviceAPI.getAreaproyectos().then(function(r){
                if(r.data.success && r.data.rows.length>0){
                    $scope.areas_proyectos=r.data.rows.map(function(i){ return {id: i.id_arp ,label: i.desc_arp};});
                }
            });

            $scope.consultar=function(){
                if($stateParams.idinventario=='' || angular.isUndefined($stateParams.idinventario))
                    return false;
                serviceAPI.getInventario({id:$stateParams.idinventario}).success(function(r){
                    if(r.success && r.num_rows){
                        r.rows.periodo=new Date(r.rows.periodo);
                        angular.extend($scope.data, r.rows);
                        $timeout(function(){
                            $scope.areas_proyectos.filter(function(item){
                                if(item.id==r.rows.id_area){
                                    $scope.data.id_area=item;
                                }
                            });
                        },4000);
                    }
                });
            };
            $scope.guardar=function(){
                serviceAPI.saveInventario($scope.data).success(function(r){
                    var typeNotify=(r.success)?'warning':'error';
                    if(r.success && r.affected_rows==1){
                        typeNotify='success';
                    }
                    toastr[typeNotify](r.messages, 'SOFTPG');
                });
            };

            $scope.listado=function(){
                $state.go('dashboard.costo_de_venta.inventarios');
            };

        }])
        .controller('inventarioListCtrl', [ '$scope', '$state', 'serviceAPI', 'toastr', function($scope, $state, serviceAPI, toastr){
            $scope.smartTablePageSize = 10;
            $scope.displayed=[];
            $scope.callServer=function(tableState){
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getInventarios(params).success(function(response){
                    if(response.success && response.totalItemCount>0){
                        $scope.displayed=response.rows;
                        tableState.pagination.numberOfPages =response.numberOfPages;
                    }    
                });
            };
            $scope.agregar=function(){
                $state.go('dashboard.costo_de_venta.inventario-details');
            };
            $scope.editar=function(id){
                $state.go('dashboard.costo_de_venta.inventario-details', {idinventario:id})
            };
            $scope.eliminar=function(id, position){

            };
        }])
})();