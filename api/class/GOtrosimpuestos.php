<?php
class GOtrosimpuestos{

    public function __construct(){
    }

    public function consultar(){
        return "SELECT DATE_FORMAT(periodo_otri, '%d/%m/%Y') AS periodo, id_arp, monto_otri AS monto, id_otri  FROM otros_impuestos WHERE id_otri=?";
    }

    public function actualizar(){
        return "UPDATE otros_impuestos SET periodo_otri=?, id_arp=?, monto_otri=? WHERE id_otri=?";
    }

    public function agregar(){
        return "INSERT INTO otros_impuestos (periodo_otri, id_arp, monto_otri, id_otri) VALUES (?, ?, ?, ?)";
    }

    public function listar(){
        return "SELECT c.id_otri, DATE_FORMAT(c.periodo_otri, '%c') AS mes, DATE_FORMAT(c.periodo_otri, '%Y') AS anio, c.monto_otri AS monto, ap.desc_arp FROM otros_impuestos AS c LEFT OUTER JOIN area_proyecto AS ap ON ap.id_arp=c.id_arp";
    }
}
?>