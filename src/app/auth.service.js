(function() {
    'use strict';

    angular.module('BlurAdmin.authService', [])
        .factory('AuthenticationService', [ '$localStorage', function($localStorage){
            var service={};
            $localStorage.$default({loggedIn: false, isAdmin:false, isCaja:false, username:false, token:'', namerol:'', states:[]});
            service.setLoggedIn=function(login){
                $localStorage.loggedIn= JSON.stringify(login);
            };

            service.setToken=function(authToken){
                $localStorage.token=authToken;
            };
            
            service.isLoggedIn=function(){
                return JSON.parse($localStorage.loggedIn);
            };
            service.setRol=function(name){
                $localStorage.namerol=name;
            };
            service.getRol=function(){
                return $localStorage.namerol;
            };
            service.setStates=function(statepermission){
                $localStorage.states=statepermission;
            };
            service.getStatesusers=function(){
                return $localStorage.states;
            };
            service.setAdmin=function(isadmin){
                $localStorage.isAdmin= JSON.stringify(isadmin);
            };
            service.isAdmin=function(){
                var uservalid=$localStorage.namerol=="ADMINISTRADOR";
                var login=JSON.parse($localStorage.loggedIn);
                return (uservalid && login);
            };
            service.setCajero=function(iscajero){
                $localStorage.isCaja= JSON.stringify(iscajero);
            };
            service.isCajero=function(){
                var uservalid=$localStorage.namerol=="CAJERO";
                var login=JSON.parse($localStorage.loggedIn);
                return (uservalid && login);
            };
            service.isSupervisor=function(){
                var uservalid=$localStorage.namerol=="SUPERVISOR";
                var login=JSON.parse($localStorage.loggedIn);
                return (uservalid && login);
            };    
            service.isSuperusuario=function(){
                var uservalid=$localStorage.namerol=="SUPERUSUARIO";
                var login=JSON.parse($localStorage.loggedIn);
                return (uservalid && login);
            };
            service.setUser = function(user){
                $localStorage.username=user;
            };
            
            service.getUser=function(){
                return $localStorage.username;
            };

            service.setToken = function(token) {
                $localStorage.token=token;
            };
    
            service.getToken = function() {
                return $localStorage.token;
            };
    
            service.signOut = function() {
                $localStorage.$reset();
            };
            return service;
        }]);

})();
