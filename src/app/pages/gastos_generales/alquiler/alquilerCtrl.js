(function(){
    'use strict';
    angular.module( 'BlurAdmin.pages.gastos_generales.alquiler',[])
        .controller('alquilerCtrl', [ '$scope', '$state','$stateParams','serviceAPI', 'toastr', function($scope, $state,$stateParams,serviceAPI, toastr){
            $scope.data={id_alq:'', id_arp:'', periodo:'', monto:0.00};
            $scope.areas_proyectos=[];
            serviceAPI.getAreaproyectos().success(function(r){
                if(r.success && r.rows.length>0){
                    $scope.areas_proyectos=r.rows.map(function(i){ return {id: i.id_arp ,label: i.desc_arp};});
                }
            });
            $scope.listado=function(){
                console.log('aqui');
                $state.go('dashboard.gastos_generales.alquiler');
            };
            $scope.consultar=function(){
                if($stateParams.idalquiler=='' || angular.isUndefined($stateParams.idalquiler))
                    return false;
                serviceAPI.getAlquiler({id:$stateParams.idalquiler}).success(function(r){
                    if(r.success && r.num_rows==1){
                        r.rows.periodo=new Date(r.rows.periodo);
                        angular.extend($scope.data, r.rows);
                        console.log($scope.areas_proyectos);
                        $scope.areas_proyectos.filter(function(item){
                            if(r.rows.id_area==item.id){
                                $scope.data.id_area=item;
                            }
                        });
                    }else{
                        toastr.error(r.messages, 'SOFTPG')
                    }
                });
            };
            $scope.guardar=function(){
                serviceAPI.savealquiler($scope.data).success(function(response){
                    var typeNotify=(response.success)?'warning':'error';
                    if(response.success && response.affected_rows==1){
                        typeNotify='success';
                    }
                    angular.extend($scope.data, $scope.default);
                    toastr[typeNotify](response.messages, 'SOFTPG');
                });
            };
        }])
        .controller('alquilerlistCtrl', ['$scope', '$state', 'serviceAPI', function($scope, $state, serviceAPI){
            $scope.smartTablePageSize = 10;
            $scope.displayed=[];
            $scope.callServer=function(tableState){
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getAlquileres(params).success(function(response){
                    if(response.success && response.totalItemCount>0){
                        $scope.displayed=response.rows;
                        tableState.pagination.numberOfPages =response.numberOfPages;
                    }    
                });
            };
            $scope.agregar=function(){
                $state.go('dashboard.gastos_generales.alquiler-detail');
            };
            $scope.editar=function(id){
                $state.go('dashboard.gastos_generales.alquiler-detail', {idalquiler:id});
            };
            $scope.eliminar=function(id, position){
                serviceAPI.eliminarAlquiler({id:id}).success(function(response){
                    if(response.success && response.affected_rows){
                        $scope.displayed=$scope.displayed.filter(function(i){ return (i.id_arp!=id); });
                    }else
                        toastr.error(response.messages, 'SOFTPG');
                });
            };
        }]);
})();