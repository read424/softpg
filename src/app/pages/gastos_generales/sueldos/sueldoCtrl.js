(function(){
    'use strict';
    angular.module('BlurAdmin.pages.gastos_generales.sueldos',[])
        .controller('sueldoCtrl', [ '$scope', '$state','$stateParams','serviceAPI', 'toastr', function($scope, $state,$stateParams,serviceAPI, toastr){
            $scope.data={id_sueldo:'', id_area:'', periodo:'', monto:0.00};
            $scope.areas_proyectos=[];
            var areas_proyectos=[];
            serviceAPI.getAreaproyectos().then(function(r){
                if(r.data.success && r.data.rows.length>0){
                    areas_proyectos=r.data.rows.map(function(i){ return {id: i.id_arp ,label: i.desc_arp};});
                    angular.copy(areas_proyectos, $scope.areas_proyectos);
                }
            });

            $scope.listado=function(){
                $state.go('dashboard.gastos_generales.sueldos');
            };
            
            $scope.consultar=function(){
                if($stateParams.idsueldo=='' || angular.isUndefined($stateParams.idsueldo))
                    return false;
                serviceAPI.getSueldo({id:$stateParams.idsueldo}).success(function(r){
                    if(r.success && r.num_rows==1){
                        r.rows.periodo=new Date(r.rows.periodo);
                        console.log($scope.areas_proyectos.length);
                        angular.forEach($scope.areas_proyectos, function(iRow){
                            console.log(iRow.id, r.rows.id_area);
                            if(iRow.id==r.rows.id_area){ r.rows.id_area=iRow; console.log(r.rows.id_area);}
                        });
                        angular.extend($scope.data, r.rows);
                    }else{
                        toastr.error(r.messages, 'SOFTPG')
                    }
                });
            };
            $scope.guardar=function(){
                serviceAPI.saveSueldos($scope.data).success(function(response){
                    var typeNotify=(response.success)?'warning':'error';
                    if(response.success && response.affected_rows==1){
                        typeNotify='success';
                    }
                    angular.extend($scope.data, $scope.default);
                    toastr[typeNotify](response.messages, 'SOFTPG');
                });
            };
        }])
        .controller('sueldoListCtrl', ['$scope', '$state', 'serviceAPI', function($scope, $state, serviceAPI){
            $scope.smartTablePageSize = 10;
            $scope.displayed=[];
            $scope.callServer=function(tableState){
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getSueldos(params).success(function(response){
                    if(response.success && response.totalItemCount>0){
                        $scope.displayed=response.rows;
                        tableState.pagination.numberOfPages =response.numberOfPages;
                    }    
                });
            };
            $scope.agregar=function(){
                $state.go('dashboard.gastos_generales.sueldos-details');
            };
            $scope.editar=function(id){
                $state.go('dashboard.gastos_generales.sueldos-details', {idsueldo:id});
            };
            $scope.eliminar=function(id, position){
                serviceAPI.eliminarsueldo({id:id}).success(function(response){
                    if(response.success && response.affected_rows){
                        $scope.displayed=$scope.displayed.filter(function(i){ return (i.id_arp!=id); });
                    }else
                        toastr.error(response.messages, 'SOFTPG');
                });
            };
        }]);
})();