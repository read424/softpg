(function(){
    'use strict';

    angular.module('BlurAdmin.pages.catalogo.areaproyecto',[])
        .directive('areaproyeExist', [ '$q', 'serviceAPI', function($q, serviceAPI){
            return {
                require: 'ngModel', transclude : true,
                scope :{ idarea:'='},
                link:function($scope, $element, $attrs, ngModel){
                    ngModel.$asyncValidators.areaproyeExist=function(modelValue, viewValue){
                        if(viewValue=='')
                            return $q.reject();
                        console.log(ngModel.$dirty)
                        if(!ngModel.$dirty)
                            return $q.resolve();
                        return serviceAPI.existAreaproy({descripcion:viewValue, id: $scope.idarea}).then(function(r){
                            if(r.data.success && r.data.num_rows!=0){
                                return $q.reject();
                            }
                            return $q.resolve();
                        });
                    }
                }
            }
        }])
        .controller('areaproyectoCtrl', [ '$scope', '$state', '$stateParams', 'serviceAPI', 'toastr', function($scope, $state, $stateParams, serviceAPI, toastr){
            $scope.default={id_areaproy:'', nom_areaproy:''};
            $scope.data={id_areaproy:'', nom_areaproy:''};
            $scope.listado=function(){
                $state.go('dashboard.catalogo.areaproyectos');
            };
            $scope.consultar=function(){
                if($stateParams.idaproyecto=='')
                    return false;
                serviceAPI.getAreaproyecto({id:$stateParams.idaproyecto}).success(function(r){
                    if(r.success && r.num_rows==1){
                        angular.extend($scope.data, r.rows);
                    }else{
                        toastr.error(r.messages, 'SOFTPG')
                    }
                });
            };
            $scope.guardar=function(){
                serviceAPI.saveAreaproyecto($scope.data).success(function(response){
                    var typeNotify=(response.success)?'warning':'error';
                    if(response.success && response.affected_rows==1){
                        typeNotify='success';
                    }
                    angular.extend($scope.data, $scope.default);
                    toastr[typeNotify](response.messages, 'SOFTPG');
                });
            };
        }])
        .controller('areaproyectoListCtrl', ['$scope', '$state', 'serviceAPI', function($scope, $state, serviceAPI){
            $scope.smartTablePageSize = 10;
            $scope.displayed=[];
            $scope.callServer=function(tableState){
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getAreaproyectos(params).success(function(response){
                    if(response.success && response.totalItemCount>0){
                        $scope.displayed=response.rows;
                        tableState.pagination.numberOfPages =response.numberOfPages;
                    }    
                });
            };
            $scope.agregar=function(){
                $state.go('dashboard.catalogo.aproyecto-detail');
            };
            $scope.editar=function(id){
                $state.go('dashboard.catalogo.aproyecto-detail', {idaproyecto:id});
            };
            $scope.eliminar=function(id, position){
                serviceAPI.eliminarAreaproyecto({id:id}).success(function(response){
                    if(response.success && response.affected_rows){
                        $scope.displayed=$scope.displayed.filter(function(i){ return (i.id_arp!=id); });
                    }else
                        toastr.error(response.messages, 'SOFTPG');
                });
            };
        }]);
})();