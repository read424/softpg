<?php
class GSueldo{

    public function __construct(){
    }

    public function consultar(){
        return "SELECT DATE_FORMAT(periodo_sue, '%d/%m/%Y') AS periodo, id_arp, monto_sue AS monto, id_sue  FROM sueldos WHERE id_sue=?";
    }

    public function actualizar(){
        return "UPDATE sueldos SET periodo_sue=?, id_arp=?, monto_sue=? WHERE id_sue=?";
    }

    public function agregar(){
        return "INSERT INTO sueldos (periodo_sue, id_arp, monto_sue, id_sue) VALUES (?, ?, ?, ?)";
    }

    public function listar(){
        return "SELECT c.id_sue, DATE_FORMAT(c.periodo_sue, '%c') AS mes, DATE_FORMAT(c.periodo_sue, '%Y') AS anio, c.monto_sue AS monto, ap.desc_arp FROM sueldos AS c LEFT OUTER JOIN area_proyecto AS ap ON ap.id_arp=c.id_arp";
    }
}
?>