<?php
class GAreaproyecto{


    public function __construct(){

    }

    public function eliminar(){
        return "DELETE FROM area_proyecto WHERE id_arp=?";
    }

    public function existsDescripcion(){
        return "SELECT id_arp FROM area_proyecto WHERE desc_arp=? AND id_arp!=?";
    }

    public function listar(){
        return "SELECT * FROM area_proyecto ORDER BY desc_arp";
    }

    public function agregar(){
        return "INSERT INTO area_proyecto (desc_arp, id_arp) VALUES (?, ?)";
    }

    public function actualizar(){
        return "UPDATE area_proyecto SET desc_arp=? WHERE id_arp=?";
    }

    public function consultar(){
        return "SELECT * FROM area_proyecto WHERE id_arp=?";
    }
}
?>