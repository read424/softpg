<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
    $response_json	=array('success'=>false, 'auth'=>false, 'admin'=>false, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
    $data = json_decode(file_get_contents('php://input'));
    include './class/GLibfunciones.php';
    $OConex=new GConector();
    $oUsuario=new GUsuario();
    $init_stmt=$OConex->stmt_init();
    switch($_GET['oper']){
        case 'consultar':
            if(!isset($data->id) || empty($data->id))
                break;
            $sql=$oUsuario->getUsuario();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('i', $data->id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $result_stmt=$init_stmt->get_result();
            $response_json['success']=true;
            $response_json['num_rows']=$result_stmt->num_rows;
            if($result_stmt->num_rows==0){
                $response_json['messages']="No existe registro de usuario";
                break;
            }
            $a_roles=array('1'=>'Administrador', '2'=>'Gerente', '3'=>'Asistente');
            $row=$result_stmt->fetch_assoc();
            $row['password']='******';
            $row['rol_usr']=$a_roles[$row['rol_usr']];
            unset($row['status'], $row['clave_usr']);
            $response_json['rows']=array_combine(array('id_usuario', 'user', 'id_rol', 'password'), array_values($row));
        break;
        case 'statuschange':
            if(!isset($data->id, $data->status) || empty($data->id))
                break;
            $sql=$oUsuario->getUsuario();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('i', $data->id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $result=$init_stmt->get_result();
            $response_json['num_rows']=$result->num_rows;
            if($result->num_rows==0){
                $response_json['messages']="Registro no se encuentra en el sistema";
                break;
            }
            $row=$result->fetch_object();
            if($row->status!=$data->status){
                $response_json['success']=TRUE;
                $response_json['messages']="Otro usuario realizo el cambio de status";
                break;
            }
            $status=($data->status=='0')?'1':'0';
            $sql=$oUsuario->updateStatusUsuario();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('si', $status, $data->id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $response_json['affected_rows']=$init_stmt->affected_rows;
            $response_json['success']=TRUE;
            $response_json['messages']=($init_stmt->affected_rows==0)?"No se realizo cambio en el status": "Se realizo satisfactoriamente el cambio de status";
            if($init_stmt->affected_rows==1)
                $response_json['rows']['status']=$status;        
        break;
        case 'guardar':
            if(!isset($data->id_usuario, $data->user, $data->password, $data->rep_password, $data->id_rol) || (empty($data->user) || empty($data->password) || empty($data->rep_password) || empty($data->id_rol) || $data->password!=$data->rep_password))
                break;
            if(empty($data->id_usuario))
                $sql=$oUsuario->agregar();
            else
                $sql=$oUsuario->actualizar();
            $data->password=md5(sha1($data->password));
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('sssi', $data->user, $data->password, $data->id_rol->id, $data->id_usuario))
                throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
            $init_stmt->execute();
            $response_json['success']=true;
            $response_json['affected_rows']=$init_stmt->affected_rows;
            $response_json['rows']['id']=(empty($data->id_usuario))?$init_stmt->insert_id:$data->id_usuario;
            $response_json['messages']=($init_stmt->affected_rows==1)?"Se registraron los datos exitosamente":"No se realizaron cambios en el registro";
        break;
        case 'existelogin':
            if(!isset($data->user, $data->id) || empty($data->user))
                break;
            $sql=$oUsuario->exitsUsuario();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('si', $data->user, $data->id))
                throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
            $init_stmt->execute();
            $result_stmt=$init_stmt->get_result();
            $response_json['success']=true;
            $response_json['num_rows']=$result_stmt->num_rows;

        break;
        case 'listar':
            $store_params=array(0=>'');
            if(isset($data->predicateObject)){
                foreach($data->predicateObject as $fields => $value){
                    $store_params[0].='s';
                    ${$fields}=sprintf("%%%s%%",$value);
                    $store_params[]=&${$fields};
                    $OBanco->addFilter($fields);
                }
            }
            $sql=$oUsuario->listar();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(count($store_params)>1){
                if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
                    throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
            }
            $init_stmt->execute();
            $result_rows=$init_stmt->get_result();
            $response_json['totalItemCount']=$result_rows->num_rows;
            $response_json['success']=true;
            if($result_rows->num_rows==0)
                break;
            if(isset($data->start, $data->number)){
                $response_json['numberOfPages']=ceil($result_rows->num_rows/$data->number);
                $Opagination=new GPagination();
                $Opagination->setInit($data->start);
                $Opagination->setLimit($data->number);
                $sql=$Opagination->prepareSQL($sql);
            }
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(count($store_params)>1){
                if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
                    throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
            }
            $init_stmt->execute();
            $result=$init_stmt->get_result();
            $i=(isset($data->start))?$data->start:0;
            $number=(isset($data->number))?$data->number:10;
            while($row=$result->fetch_assoc()){
                array_push($response_json['rows'], array_merge($row, array('item'=>++$i)));
            }
            $response_json['totalItemCount']=count($response_json['rows']);        
        break;
    }
    echo json_encode($response_json);
    
}catch(Exception $e){

}
?>