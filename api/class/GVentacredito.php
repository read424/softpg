<?php
class GVentacredito extends GVentas{

    public function __construct(){
        parent::__construct();
    }

    public function consultar(){
        return "SELECT DATE_FORMAT(periodo_vtacred, '%d/%m/%Y') AS periodo, id_arp, monto_vtacred AS monto, id_vtacred  FROM credito WHERE id_vtacred=?";
    }

    public function actualizar(){
        return "UPDATE credito SET periodo_vtacred=?, id_arp=?, monto_vtacred=? WHERE id_vtacred=?";
    }

    public function agregar(){
        return "INSERT INTO credito (periodo_vtacred, id_arp, monto_vtacred, id_vtacred) VALUES (?, ?, ?, ?)";
    }

    public function listar(){
        return "SELECT c.id_vtacred, DATE_FORMAT(c.periodo_vtacred, '%c') AS mes, DATE_FORMAT(c.periodo_vtacred, '%Y') AS anio, c.monto_vtacred AS monto, ap.desc_arp FROM credito AS c LEFT OUTER JOIN area_proyecto AS ap ON ap.id_arp=c.id_arp";
    }
}
?>