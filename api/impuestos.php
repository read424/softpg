<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
    $a_oper=array("listar", "guardar", "consultar", "changedefault", "eliminar");
    if(!isset($_GET['oper']) || !in_array($_GET['oper'], $a_oper)){
        die();
    }
    $response_json	=array('success'=>false, 'auth'=>false, 'admin'=>false, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
    $data = json_decode(file_get_contents('php://input'));
    include './class/GLibfunciones.php';
    $OConex=new GConector();
    $init_stmt=$OConex->stmt_init();
    $oImpuesto=new GImpuesto();
    switch($_GET['oper']){
        case 'changedefault':
            if(!isset($data->id, $data->status) || empty($data->id))
                break;
            $sql=$oImpuesto->consultar();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('i', $data->id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $result=$init_stmt->get_result();
            $response_json['num_rows']=$result->num_rows;
            if($result->num_rows==0){
                $response_json['messages']="Registro no se encuentra en el sistema";
                break;
            }
            $row=$result->fetch_object();
            if($row->activo_imp!=$data->status){
                $response_json['success']=TRUE;
                $response_json['messages']="Otro usuario realizo el cambio de status";
                break;
            }
            $status=($data->status=='0')?'1':'0';
            $sql=$oImpuesto->updateDefault();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('i', $data->id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $response_json['affected_rows']=$init_stmt->affected_rows;
            $response_json['success']=TRUE;
            $response_json['messages']=($init_stmt->affected_rows==0)?"No se realizo cambio en el status": "Se realizo satisfactoriamente el cambio de status";
            if($init_stmt->affected_rows==1){
               $sql=$oImpuesto->updatenotDefault();
               if(!$init_stmt->prepare($sql))
                   throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
               if(!$init_stmt->bind_param('i', $data->id))
                   throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
                $init_stmt->execute();
                $response_json['rows']['status']=$status;
            }
        break;
        case 'eliminar':
            if(!isset($data->id) || empty($data->id))
                break;
            $sql=$oImpuesto->eliminar();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('i', $data->id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $response_json['success']=true;
            $response_json['affected_rows']=$init_stmt->affected_rows;
            if($response_json['affected_rows']!=1)
                $response_json['messages']="No se pudo eliminar la información";
        break;
        case 'consultar':
            if(!isset($data->id) || empty($data->id))
                break;
            $sql=$oImpuesto->consultar();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('i', $data->id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $result_stmt=$init_stmt->get_result();
            $response_json['success']=true;
            $response_json['num_rows']=$result_stmt->num_rows;
            if($result_stmt->num_rows==1){
                $row=$result_stmt->fetch_assoc();
                $response_json['rows']=array_combine(array('factor_imp', 'status', 'id_imp'), array_values($row));
            }else
                $response_json['messages']="No se encontraron registros que coincida con la consulta";
        break;
        case 'guardar':
            if(!isset($data->id_imp, $data->factor_imp, $data->status) || empty($data->factor_imp))
                break;
            $sql=call_user_func(array($oImpuesto, (empty($data->id_imp))?'agregar':'actualizar'));
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('sii', $data->factor_imp, $data->status, $data->id_imp))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $response_json['success']=true;
            $response_json['affected_rows']=$init_stmt->affected_rows;
            $response_json['rows']['id']=(empty($data->id_imp))?$init_stmt->insert_id:$data->id_imp;
            if($init_stmt->affected_rows!=1){
                $response_json['messages']=(empty($data->id_imp))?"No se pudo registrar los datos":"No ocurrio cambios en el registro";
                break;
            }else
                $response_json['messages']="Los datos fueron registrados satisfactoriamente";
            if(!empty($data->id_imp))
                break;
            $sql="UPDATE impuestos SET activo_imp=0 WHERE id_imp!=?";
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('i', $init_stmt->insert_id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
        break;
        case 'listar':
            $store_params=array(0=>'');
            if(isset($data->predicateObject)){
                foreach($data->predicateObject as $fields => $value){
                    $store_params[0].='s';
                    ${$fields}=sprintf("%%%s%%",$value);
                    $store_params[]=&${$fields};
                    $oImpuesto->addFilter($fields);
                }
            }
            $sql=$oImpuesto->listar();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(count($store_params)>1){
                if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
                    throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
            }
            $init_stmt->execute();
            $result_rows=$init_stmt->get_result();
            $response_json['totalItemCount']=$result_rows->num_rows;
            $response_json['success']=true;
            if($result_rows->num_rows==0)
                break;
            if(isset($data->start, $data->number)){
                $response_json['numberOfPages']=ceil($result_rows->num_rows/$data->number);
                $Opagination=new GPagination();
                $Opagination->setInit($data->start);
                $Opagination->setLimit($data->number);
                $sql=$Opagination->prepareSQL($sql);
            }
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(count($store_params)>1){
                if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
                    throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
            }
            $init_stmt->execute();
            $result=$init_stmt->get_result();
            $i=(isset($data->start))?$data->start:0;
            $number=(isset($data->number))?$data->number:10;
            while($row=$result->fetch_assoc()){
                array_push($response_json['rows'], array_merge($row, array('item'=>++$i)));
            }
            $response_json['totalItemCount']=count($response_json['rows']);
        break;
    }
    echo json_encode($response_json);
}catch(Exception $e){
    echo $e->getOutMsg();
}
?>