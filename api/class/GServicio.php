<?php
class GServicio{

    public function __construct(){
    }

    public function consultar(){
        return "SELECT DATE_FORMAT(periodo_srv, '%d/%m/%Y') AS periodo, id_arp, monto_srv AS monto, id_srv  FROM servicios WHERE id_srv=?";
    }

    public function actualizar(){
        return "UPDATE servicios SET periodo_srv=?, id_arp=?, monto_srv=? WHERE id_srv=?";
    }

    public function agregar(){
        return "INSERT INTO servicios (periodo_srv, id_arp, monto_srv, id_srv) VALUES (?, ?, ?, ?)";
    }

    public function listar(){
        return "SELECT c.id_srv, DATE_FORMAT(c.periodo_srv, '%c') AS mes, DATE_FORMAT(c.periodo_srv, '%Y') AS anio, c.monto_srv AS monto, ap.desc_arp FROM servicios AS c LEFT OUTER JOIN area_proyecto AS ap ON ap.id_arp=c.id_arp";
    }
}
?>