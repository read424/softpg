(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.ventas', [
      'BlurAdmin.pages.ventas.contado',
      'BlurAdmin.pages.ventas.credito'
    ]).config([ '$stateProvider',  function($stateProvider){
        $stateProvider.state('dashboard.ventas', {
          url: '/ventas',
          abstract: true,
          template : '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          title: 'Ventas',
          sidebarMeta: {
            icon: 'fa fa-shopping-cart',
            order: 100
          }
        }).state('dashboard.ventas.contado', {
          url: '/contado',
          templateUrl: 'app/pages/ventas/ventas_contado/list/ventascontado.html',
          title: 'Ventas Contado',
          controller: 'ventascontadoListCtrl',
          sidebarMeta: {
            order: 200
          }
        }).state('dashboard.ventas.contado-details',{
          url: '/contado/details/:idventa',
          templateUrl: 'app/pages/ventas/ventas_contado/venta_contado.html',
          controller: 'ventascontadoCtrl',
          controllerAs: 'vm',
          title: 'Venta Contado',
          params: {
            idventa: ''
          }
        }).state('dashboard.ventas.credito',{
          url: '/credito',
          templateUrl: 'app/pages/ventas/ventas_credito/list/ventascredito.html',
          controller: 'creditoListCtrl',
          title: 'Venta Credito',
          sidebarMeta: {
            order: 300
          }
        }).state('dashboard.ventas.credito-detail',{
          url: '/credito/details/:idcredito',
          templateUrl: 'app/pages/ventas/ventas_credito/ventas_credito.html',
          controller: 'creditoCtrl',
          title: 'Venta Credito',
          params: {
            idcredito: ''
          }
        });

    }]);

  })();
  