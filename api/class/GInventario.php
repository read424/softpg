<?php
class GInventario{

    public function __construct(){
    }

    public function consultar(){
        return "SELECT DATE_FORMAT(periodo_ii_inv, '%d/%m/%Y') AS periodo, id_arp, monto_ii_inv AS monto_ini, monto_if_inv AS monto_fin, id_inv FROM inventarios WHERE id_inv=?";
    }

    public function actualizar(){
        return "UPDATE inventarios SET periodo_ii_inv=?, monto_ii_inv=?, periodo_if_inv=?, monto_if_inv=?, id_arp=? WHERE id_inv=?";
    }

    public function agregar(){
        return "INSERT INTO inventarios (periodo_ii_inv, monto_ii_inv, periodo_if_inv, monto_if_inv, id_arp, id_inv) VALUES (?, ?, ?, ?, ?, ?)";
    }

    public function listar(){
        return "SELECT i.id_inv, DATE_FORMAT(i.periodo_ii_inv, '%c') AS mes_ii, DATE_FORMAT(i.periodo_ii_inv, '%Y') AS anio_ii, DATE_FORMAT(i.periodo_if_inv, '%c') AS mes_if, DATE_FORMAT(i.periodo_if_inv, '%Y') AS anio_if, i.monto_ii_inv AS monto_ini, i.monto_if_inv AS monto_fin, ap.desc_arp FROM inventarios AS i LEFT OUTER JOIN area_proyecto AS ap ON ap.id_arp=i.id_arp";
    }
}
?>