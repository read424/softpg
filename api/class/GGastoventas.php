<?php
class GGastoventas {

    public function __construct(){
    }

    public function consultar(){
        return "SELECT DATE_FORMAT(c.periodo_gv, '%d/%m/%Y') AS periodo, c.id_arp, c.monto_gv AS monto, c.id_gv FROM gasto_ventas AS c WHERE id_gv=? ";
    }

    public function actualizar(){
        return "UPDATE gasto_ventas SET periodo_gv=?, id_arp=?, monto_gv=? WHERE id_gv=?";
    }

    public function agregar(){
        return "INSERT INTO gasto_ventas (periodo_gv, id_arp, monto_gv, id_gv) VALUES (?, ?, ?, ?)";
    }

    public function listar(){
        return "SELECT c.id_gv, DATE_FORMAT(c.periodo_gv, '%c') AS mes, DATE_FORMAT(c.periodo_gv, '%Y') AS anio, c.monto_gv AS monto, ap.desc_arp FROM gasto_ventas AS c LEFT OUTER JOIN area_proyecto AS ap ON ap.id_arp=c.id_arp";
    }
}
?>