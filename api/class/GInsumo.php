<?php
class GInsumo{

    public function __construct(){
    }

    public function consultar(){
        return "SELECT DATE_FORMAT(periodo_inu, '%d/%m/%Y') AS periodo, id_arp, monto_inu AS monto, id_inu  FROM insumos_utiles WHERE id_inu=?";
    }

    public function actualizar(){
        return "UPDATE insumos_utiles SET periodo_inu=?, id_arp=?, monto_inu=? WHERE id_inu=?";
    }

    public function agregar(){
        return "INSERT INTO insumos_utiles (periodo_inu, id_arp, monto_inu, id_inu) VALUES (?, ?, ?, ?)";
    }

    public function listar(){
        return "SELECT c.id_inu, DATE_FORMAT(c.periodo_inu, '%c') AS mes, DATE_FORMAT(c.periodo_inu, '%Y') AS anio, c.monto_inu AS monto, ap.desc_arp FROM insumos_utiles AS c LEFT OUTER JOIN area_proyecto AS ap ON ap.id_arp=c.id_arp";
    }
}
?>