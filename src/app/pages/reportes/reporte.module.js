(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.reportes', [
        'BlurAdmin.pages.reporte'
    ]).config([ '$stateProvider',  function($stateProvider){
        $stateProvider.state('dashboard.reportes', {
          url: '/reporte',
          abstract: true,
          template : '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          title: 'Reporte',
          sidebarMeta: {
            icon: 'fa fa-pie-chart',
            order: 600
          }
        }).state('dashboard.reportes.estado_resultados', {
          url:'/reporte/estado_resultados',
          templateUrl:'app/pages/reportes/estadoresultados/reporte.html',
          title: 'Estado de Resultados',
          controller: 'reporteCtrl',
          sidebarMeta: {
            order: 100
          }
        }).state('dashboard.reportes.estado_resultados-details', {
          url:'/reporte/estado_resultados/:id_er/:id_area',
          templateUrl:'app/pages/reportes/estadoresultados/reporte-details.html',
          title: 'Estado de Resultados',
          controller: 'reporteviewCtrl',
          params: {
            id_er: '',
            id_area: ''
          },
        });
    }]);

  })();
  