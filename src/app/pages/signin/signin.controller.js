(function() {
    'use strict';

    angular.module('BlurAdmin.signin')
        .controller('SignInCtrl', ['$scope', '$state', 'AuthenticationService', 'toastr', 'serviceAPI', 'commonService', function($scope, $state, AuthenticationService, toastr, serviceAPI, commonService){
            $scope.data={username:'', clave:''};
            $scope.signIn = function() {
                serviceAPI.signIn($scope.data).success(function(r){
                    AuthenticationService.setLoggedIn(r.auth);
                    if(r.auth){
                        AuthenticationService.setUser($scope.data.username);
                        $state.go('dashboard');
                    }else{
                        toastr.error(r.messages, 'Acceso al Sistema');
                    }
                });
            };  
        }]);

})();
