(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.costo_de_venta', [
      'BlurAdmin.pages.costo_de_venta.gasto_de_ventas',
      'BlurAdmin.pages.costo_de_venta.inventario'
    ]).config([ '$stateProvider',  function($stateProvider){
        $stateProvider.state('dashboard.costo_de_venta', {
          url: '/costo_de_venta',
          abstract: true,
          template : '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          title: 'Costo de Ventas',
          sidebarMeta: {
            icon: 'fa fa-usd',
            order: 200
          }
        }).state('dashboard.costo_de_venta.gasto_de_ventas', {
          url: '/costo_de_venta',
          templateUrl: 'app/pages/costo_de_venta/gasto_de_ventas/list/gastos_de_venta.html',
          title: 'Gasto Ventas',
          controller: 'gasto_ventasListCtrl',
          sidebarMeta: {
            order: 100
          }
        }).state('dashboard.costo_de_venta.gastoventa-detail',{
          url: '/gastoventa/details/:idgastoventa',
          templateUrl: 'app/pages/costo_de_venta/gasto_de_ventas/gasto_de_ventas.html',
          title: 'Gasto Ventas',
          controller: 'gasto_ventasCtrl',
          params: {
            idgastoventa: ''
          }
        })
        .state('dashboard.costo_de_venta.inventarios',{
          url: '/inventarios',
          templateUrl: 'app/pages/costo_de_venta/inventario/list/inventarioList.html',
          title: 'Inventarios',
          controller: 'inventarioListCtrl',
          sidebarMeta: {
            order: 200
          }
        })
        .state('dashboard.costo_de_venta.inventario-details', {
          url: '/inventarios/details/:idinventario',
          templateUrl: 'app/pages/costo_de_venta/inventario/inventario.html',
          title: 'Inventarios',
          controller: 'inventarioCtrl',
          params:{
            idinventario:''
          }
        });
    }]);

  })();
  