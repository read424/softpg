<?php
class GImpuesto{

    public function __construct(){
    }

    public function consultar(){
        return "SELECT factor_imp, activo_imp, id_imp FROM impuestos WHERE id_imp=?";
    }

    public function actualizar(){
        return "UPDATE impuestos SET factor_imp=?, activo_imp=? WHERE id_imp=?";
    }

    public function updatenotDefault(){
        return "UPDATE impuestos SET activo_imp=0 WHERE id_imp!=?";
    }
    
    public function updateDefault(){
        return "UPDATE impuestos SET activo_imp=1 WHERE id_imp=?";
    }

    public function agregar(){
        return "INSERT INTO impuestos (factor_imp, activo_imp, id_imp) VALUES (?, ?, ?)";
    }

    public function listar(){
        return "SELECT i.id_imp, i.factor_imp, i.activo_imp FROM impuestos AS i";
    }
}
?>