(function(){
    'use strict';
    angular.module( 'BlurAdmin.pages.gastos_generales.otros_impuestos',[])
        .controller('otros_impuestosCtrl', [ '$scope', '$state','$stateParams','serviceAPI', 'toastr', function($scope, $state,$stateParams,serviceAPI, toastr){
            $scope.default={id_otri:'', id_area:'', periodo:'', monto:0.00};
            $scope.data={id_otri:'', id_area:'', periodo:'', monto:0.00};
            $scope.areas_proyectos=[];
            serviceAPI.getAreaproyectos().success(function(r){
                if(r.success && r.rows.length>0){
                    $scope.areas_proyectos=r.rows.map(function(i){ return {id: i.id_arp ,label: i.desc_arp};});
                }
            });
         
            $scope.listado=function(){
                $state.go('dashboard.gastos_generales.otros_impuestos');
            };
            $scope.consultar=function(){
                if($stateParams.idimpuesto=='' || angular.isUndefined($stateParams.idimpuesto))
                    return false;
                serviceAPI.getOtroimpuesto({id:$stateParams.idimpuesto}).success(function(r){
                    if(r.success && r.num_rows==1){
                        r.rows.periodo=new Date(r.rows.periodo);
                        angular.extend($scope.data, r.rows);
                        $scope.areas_proyectos.filter(function(item){
                            if(r.rows.id_area==item.id){
                                $scope.data.id_area=item;
                            }
                        });
                    }else{
                        toastr.error(r.messages, 'SOFTPG')
                    }
                });
            };
            $scope.guardar=function(){
                serviceAPI.saveImpuestos($scope.data).success(function(response){
                    var typeNotify=(response.success)?'warning':'error';
                    if(response.success && response.affected_rows==1){
                        typeNotify='success';
                    }
                    angular.extend($scope.data, $scope.default);
                    toastr[typeNotify](response.messages, 'SOFTPG');
                });
            };
        }])
        .controller('otros_impuestosListCtrl', ['$scope', '$state', 'serviceAPI', function($scope, $state, serviceAPI){
            $scope.smartTablePageSize = 10;
            $scope.displayed=[];
            $scope.callServer=function(tableState){
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getOtrosImpuestos(params).success(function(response){
                    if(response.success && response.totalItemCount>0){
                        $scope.displayed=response.rows;
                        tableState.pagination.numberOfPages =response.numberOfPages;
                    }    
                });
            };
            $scope.agregar=function(){
                $state.go('dashboard.gastos_generales.otroimpuesto-detail');
            };
            $scope.editar=function(id){
                $state.go('dashboard.gastos_generales.otroimpuesto-detail', {idimpuesto:id});
            };
            $scope.eliminar=function(id, position){
                serviceAPI.eliminarImpuesto({id:id}).success(function(response){
                    if(response.success && response.affected_rows){
                        $scope.displayed=$scope.displayed.filter(function(i){ return (i.id_arp!=id); });
                    }else
                        toastr.error(response.messages, 'SOFTPG');
                });
            };
        }]);
})();