(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.catalogo', [
      'BlurAdmin.pages.catalogo.areaproyecto',
      'BlurAdmin.pages.catalogo.impuestos',
      'BlurAdmin.pages.catalogo.usuarios'
    ]).config([ '$stateProvider',  function($stateProvider){
        $stateProvider.state('dashboard.catalogo', {
          url: '/catalogo',
          abstract: true,
          template : '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          title: 'Gestion de Sistema',
          sidebarMeta: {
            icon: 'fa fa-book',
            order: 100
          }
        }).state('dashboard.catalogo.usuarios', {
          url: '/usuarios',
          templateUrl: 'app/pages/catalogo/usuarios/list/usuariosList.html',
          controller: "UsuarioListCtrl",
          title: 'Usuarios',
          sidebarMeta: {
            order: 100
          }
        }).state('dashboard.catalogo.usuario-detail',{
          url: '/usuarios/details/:idusuario',
          templateUrl: 'app/pages/catalogo/usuarios/usuario.html',
          controller: 'UsuarioCtrl',
          title: 'Usuario',
          params: {
            idusuario: ''
          }
        }).state('dashboard.catalogo.areaproyectos', {
          url: '/areaproyectos',
          templateUrl: 'app/pages/catalogo/area_proyecto/list/areaproyectoList.html',
          title: 'Area Proyectos',
          controller: 'areaproyectoListCtrl',
          sidebarMeta: {
            order: 200
          }
        }).state('dashboard.catalogo.aproyecto-detail',{
          url: '/areaproyectos/details/:idaproyecto',
          templateUrl: 'app/pages/catalogo/area_proyecto/areaproyecto.html',
          title: 'Area Proyecto',
          controller: 'areaproyectoCtrl',
          params: {
            idaproyecto: ''
          }
        }).state('dashboard.catalogo.impuestos', {
          url: '/impuestos',
          templateUrl: 'app/pages/catalogo/impuestos/list/impuestosList.html',
          title: 'Impuestos',
          controller: 'impuestosListCtrl',
          sidebarMeta: {
            order: 300
          }
        }).state('dashboard.catalogo.impuesto-detail',{
          url: '/impuestos/details/:idimpuesto',
          templateUrl: 'app/pages/catalogo/impuestos/impuesto.html',
          title: 'Impuesto',
          controller: 'impuestosCtrl',
          params: {
            idimpuesto: ''
          }
        });

    }]);

  })();
  