(function(){
    'use strict';
    angular.module('BlurAdmin.pages.catalogo.usuarios',[])
        .directive('usuarioExist', [ '$q', 'serviceAPI', function($q, serviceAPI){
            return {
                require: 'ngModel',
                transclude : true,
                scope :{ idusuario:'='},
                link:function($scope, $element, $attrs, ngModel){
                    ngModel.$asyncValidators.usuarioExist=function(modelValue, viewValue){
                        if(ngModel.$dirty && viewValue)
                            return $.reject();
                        if(!ngModel.$dirty)
                            return $q.resolve();
                        return serviceAPI.existUsuario({user:viewValue, id: $scope.idusuario}).then(function(r){
                            if(r.data.success && r.data.num_rows!=0){
                                return $q.reject();
                            }
                            return $q.resolve();
                        });
                    }
                }
            }
        }])
        .directive('equalTo', [function(){
            return {
                require: 'ngModel',
                scope:{ password:"="},
                link:function($scope, $element, $attrs, ngModel){
                    ngModel.$validators.equalTo=function(modelValue, viewValue){
                        return (viewValue==$scope.password);
                    }
                }
            }
        }])
        .controller('UsuarioCtrl', ['$scope', '$state', '$stateParams', 'serviceAPI', 'toastr', function($scope, $state, $stateParams, serviceAPI, toastr){
            $scope.data={id_usuario:'', user:'', password:'', rep_password:'', id_rol:{}};
            $scope.roles=[
                { id:'1', label:'Administrador'},
                { id:'2', label:'Gerente'},
                { id:'3', label:'Asistente'}
            ];
            $scope.data.id_rol=$scope.roles[0];
            $scope.consultar=function(){
                if($stateParams.idusuario=='')
                    return true;
                serviceAPI.consultarUsuario({id:$stateParams.idusuario}).success(function(response){
                    var typeNotify=(response.success)?'warning':'error';
                    if(response.success && response.num_rows==1){
                        typeNotify='';
                        angular.extend($scope.data, response.rows);
                        $scope.data.rep_password=response.rows.password;
                    }
                    if(typeNotify!='')
                        toastr[typeNotify](response.messages, 'SOFTPG');
                });
            };
            $scope.guardar=function(){
                serviceAPI.guardarUsuario($scope.data).success(function(response){
                    var typeNotify=(response.success)?'warning':'error';
                    if(response.success && response.affected_rows==1){
                        typeNotify='success';
                    }
                    toastr[typeNotify](response.messages, 'SISPG');
                });
            };
        }])
        .controller('UsuarioListCtrl', [ '$scope', '$state', 'serviceAPI', function($scope, $state, serviceAPI){
            $scope.smartTablePageSize = 10;
            $scope.displayed=[];
            $scope.callServer=function(tableState){
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getUsuarios(params).success(function(response){
                    if(response.success && response.totalItemCount>0){
                        $scope.displayed=response.rows;
                        tableState.pagination.numberOfPages =response.numberOfPages;
                    }    
                });
            };

            $scope.change_status=function(id_usuario, actividad, index){
                serviceAPI.statusUsuario({id:id_usuario, status:actividad}).success(function(r){
                    var typenotify='error';
                    if(r.num_rows==0){
                        typenotify='warning';
                        $scope.displayed=$scope.displayed.filter(function(i){ return (i.idusuario!=id_usuario);})
                    }
                    if(r.affected_rows==1){
                        typenotify='success';
                        $scope.displayed[index].status=r.rows.status;
                    }
                    toastr[typenotify](r.messages, 'Listado de Usuarios');
                });
            };
                
            $scope.editar=function(id){
                $state.go('dashboard.catalogo.usuario-detail', {idusuario: id});
            };
            $scope.agregar=function(){
                $state.go('dashboard.catalogo.usuario-detail');
            };
    
        }]);
})();