<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
    $a_oper=array("listar", "guardar", "consultar", "statuschange", "existareaproye", "eliminar");
    if(!isset($_GET['oper']) || !in_array($_GET['oper'], $a_oper)){
        die();
    }
    $response_json	=array('success'=>false, 'auth'=>false, 'admin'=>false, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
    $data = json_decode(file_get_contents('php://input'));
    include './class/GLibfunciones.php';
    $OConex=new GConector();
    $init_stmt=$OConex->stmt_init();
    $oVentacont=new GVentacontado();
    switch($_GET['oper']){
        case 'eliminar':
            if(!isset($data->id) || empty($data->id))
                break;
            $sql=$oVentacont->eliminar();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('i', $data->id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $response_json['success']=true;
            $response_json['affected_rows']=$init_stmt->affected_rows;
            if($response_json['affected_rows']!=1)
                $response_json['messages']="No se pudo eliminar la información";
        break;
        case 'consultar':
            if(!isset($data->id) || empty($data->id))
                break;
            $sql=$oVentacont->consultar();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('i', $data->id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $result_stmt=$init_stmt->get_result();
            $response_json['success']=true;
            $response_json['num_rows']=$result_stmt->num_rows;
            if($result_stmt->num_rows==1){
                $row=$result_stmt->fetch_assoc();
                $response_json['rows']=array_combine(array('periodo', 'id_area', 'monto', 'id_vtacontado'), array_values($row));
            }else
                $response_json['messages']="No se encontraron registros que coincida con la consulta";
        break;
        case 'guardar':
            if(!isset($data->id_area, $data->id_vtacontado, $data->monto, $data->periodo) || empty($data->id_area) || empty($data->monto) || empty($data->periodo))
                break;
            $fecha=DateTime::createFromFormat('Y-m-d\TH:i:s.uP', $data->periodo);
            if(!($fecha instanceof DateTime))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", "No es un formato de fecha valido", '-----');
            $data->periodo=$fecha->format('Y-m-d');
            $sql=call_user_func(array($oVentacont, (empty($data->id_vtacontado))?'agregar':'actualizar'));
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('sidi', $data->periodo, $data->id_area->id, $data->monto, $data->id_vtacontado))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $response_json['success']=true;
            $response_json['affected_rows']=$init_stmt->affected_rows;
            $response_json['rows']['id']=(empty($data->id_vtacontado))?$init_stmt->insert_id:$data->id_vtacontado;
            if($init_stmt->affected_rows!=1){
                $response_json['messages']=(empty($data->id_vtacontado))?"No se pudo registrar los datos":"No ocurrio cambios en el registro";
                break;
            }
            $response_json['messages']="Los datos fueron registrados satisfactoriamente";
            $sql="CALL registro_ventas(?, ?)";
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('si', $data->periodo, $data->id_area->id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $sql="CALL generate_estado_resultado(?, ?)";
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('si', $data->periodo, $data->id_area->id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
        break;
        case 'listar':
            $store_params=array(0=>'');
            if(isset($data->predicateObject)){
                foreach($data->predicateObject as $fields => $value){
                    $store_params[0].='s';
                    ${$fields}=sprintf("%%%s%%",$value);
                    $store_params[]=&${$fields};
                    $oVentacont->addFilter($fields);
                }
            }
            $sql=$oVentacont->listar();
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(count($store_params)>1){
                if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
                    throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
            }
            $init_stmt->execute();
            $result_rows=$init_stmt->get_result();
            $response_json['totalItemCount']=$result_rows->num_rows;
            $response_json['success']=true;
            if($result_rows->num_rows==0)
                break;
            if(isset($data->start, $data->number)){
                $response_json['numberOfPages']=ceil($result_rows->num_rows/$data->number);
                $Opagination=new GPagination();
                $Opagination->setInit($data->start);
                $Opagination->setLimit($data->number);
                $sql=$Opagination->prepareSQL($sql);
            }
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(count($store_params)>1){
                if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
                    throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
            }
            $init_stmt->execute();
            $result=$init_stmt->get_result();
            $i=(isset($data->start))?$data->start:0;
            $number=(isset($data->number))?$data->number:10;
            $a_meses=array(1=>"Enero", 2=>"Febrero", 3=>"Marzo", 4=>"Abril", 5=>"Mayo", 6=>"Junio", 7=>"Julio", 8=>"Agosto", 9=>"Septiembre", 10=>"Octubre", 11=>"Noviembre", 12=>"Diciembre");
            while($row=$result->fetch_assoc()){
                $row['mes']=$a_meses[$row['mes']];
                array_push($response_json['rows'], array_merge($row, array('item'=>++$i)));
            }
            $response_json['totalItemCount']=count($response_json['rows']);
        break;
    }
    echo json_encode($response_json);
}catch(Exception $e){
    echo $e->getOutMsg();
}
?>