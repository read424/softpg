(function(){
    'use strict';
    angular.module('BlurAdmin.pages.gastos_generales.servicio',[])
        .controller('serviciosCtrl', [ '$scope', '$state','serviceAPI','$stateParams', 'toastr', function($scope, $state,serviceAPI,$stateParams, toastr){
            $scope.default={id_srv:'', id_area:'', periodo:'', monto:0.00};
            $scope.data={id_srv:'', id_area:'', periodo:'', monto:0.00};
            $scope.areas_proyectos=[];
            serviceAPI.getAreaproyectos().success(function(r){
                if(r.success && r.rows.length>0){
                    $scope.areas_proyectos=r.rows.map(function(i){ return {id: i.id_arp ,label: i.desc_arp};});
                }
            });

            $scope.listado=function(){
                $state.go('dashboard.gastos_generales.servicio');
            };
            $scope.consultar=function(){
                if($stateParams.idservicio=='' || angular.isUndefined($stateParams.idservicio))
                    return false;
                serviceAPI.getservicios({id:$stateParams.idservicio}).success(function(r){
                    if(r.success && r.num_rows==1){
                        r.rows.periodo=new Date(r.rows.periodo);
                        angular.extend($scope.data, r.rows);
                        console.log($scope.areas_proyectos);
                        $scope.areas_proyectos.filter(function(item){
                            if(r.rows.id_area==item.id){
                                console.log(item);
                                $scope.data.id_area=item;
                            }
                        });
                    }else{
                        toastr.error(r.messages, 'SOFTPG')
                    }
                });
            };
            $scope.guardar=function(){
                serviceAPI.saveServivio($scope.data).success(function(response){
                    var typeNotify=(response.success)?'warning':'error';
                    if(response.success && response.affected_rows==1){
                        typeNotify='success';
                    }
                    angular.extend($scope.data, $scope.default);
                    toastr[typeNotify](response.messages, 'SOFTPG');
                });
            };
        }])
        .controller('servicioslistCtrl', ['$scope', '$state', 'serviceAPI', function($scope, $state, serviceAPI){
            $scope.smartTablePageSize = 10;
            $scope.displayed=[];
            $scope.callServer=function(tableState){
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getServicio(params).success(function(response){
                    if(response.success && response.totalItemCount>0){
                        $scope.displayed=response.rows;
                        tableState.pagination.numberOfPages =response.numberOfPages;
                    }    
                });
            };
            $scope.agregar=function(){
                $state.go('dashboard.gastos_generales.servicio-detail');
            };
            $scope.editar=function(id){
                $state.go('dashboard.gastos_generales.servicio-detail', {idservicio:id});
            };
            $scope.eliminar=function(id, position){
                serviceAPI.eliminarServicio({id:id}).success(function(response){
                    if(response.success && response.affected_rows){
                        $scope.displayed=$scope.displayed.filter(function(i){ return (i.id_arp!=id); });
                    }else
                        toastr.error(response.messages, 'SOFTPG');
                });
            };
        }]);
})();