-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-10-2018 a las 21:04:00
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.0.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sisestadopg`
--
CREATE DATABASE IF NOT EXISTS `sisestadopg` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `sisestadopg`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alquiler`
--

DROP TABLE IF EXISTS `alquiler`;
CREATE TABLE `alquiler` (
  `id_alq` int(11) NOT NULL,
  `periodo_alq` date DEFAULT NULL,
  `monto_alq` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Disparadores `alquiler`
--
DROP TRIGGER IF EXISTS `add_mto_alquiler`;
DELIMITER $$
CREATE TRIGGER `add_mto_alquiler` AFTER INSERT ON `alquiler` FOR EACH ROW BEGIN
	DECLARE exist_periodo_area INT DEFAULT 0;
	DECLARE lc_id_gg INT DEFAULT NULL;
	SELECT id_gg INTO lc_id_gg FROM gastos_generales WHERE id_arp=NEW.id_arp AND DATE_FORMAT( periodo_gg, '%m-%Y') = DATE_FORMAT( NEW.periodo_alq, '%m-%Y');
	SELECT FOUND_ROWS() INTO exist_periodo_area ;
	IF exist_periodo_area!=1 THEN 
		INSERT INTO gastos_generales (periodo_gg, id_alq, monto_alq, id_arp, monto_gg) VALUES (NEW.periodo_alq, NEW.id_alq, NEW.monto_alq, NEW.id_arp, NEW.monto_alq);
	ELSE 
		UPDATE gastos_generales SET id_alq=NEW.id_alq, monto_alq=NEW.monto_alq, monto_gg=monto_gg+NEW.monto_alq WHERE id_vta=lc_id_vta;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area_proyecto`
--

DROP TABLE IF EXISTS `area_proyecto`;
CREATE TABLE `area_proyecto` (
  `id_arp` int(11) NOT NULL,
  `desc_arp` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `area_proyecto`
--

INSERT IGNORE INTO `area_proyecto` (`id_arp`, `desc_arp`) VALUES
(7, 'Proyecto A'),
(8, 'Proyecto B'),
(9, 'Proyecto C'),
(10, 'Proyecto D');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contado`
--

DROP TABLE IF EXISTS `contado`;
CREATE TABLE `contado` (
  `id_vtacont` int(11) NOT NULL,
  `periodo_vtacont` date DEFAULT NULL,
  `monto_vtacont` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `contado`
--

INSERT IGNORE INTO `contado` (`id_vtacont`, `periodo_vtacont`, `monto_vtacont`, `id_arp`) VALUES
(1, '2018-10-12', '120.00', 7),
(2, '2018-09-18', '80.00', 9),
(3, '2018-09-26', '500.00', 7),
(4, '2018-06-06', '1000.00', 8);

--
-- Disparadores `contado`
--
DROP TRIGGER IF EXISTS `add_sum_ventacont`;
DELIMITER $$
CREATE TRIGGER `add_sum_ventacont` AFTER INSERT ON `contado` FOR EACH ROW BEGIN
	DECLARE exist_periodo_area INT DEFAULT 0;
	DECLARE lc_id_vta INT DEFAULT NULL;
	SELECT id_vta INTO lc_id_vta FROM ventas WHERE id_arp=NEW.id_arp AND DATE_FORMAT( periodo_vta, '%m-%Y') = DATE_FORMAT( NEW.periodo_vtacont, '%m-%Y');
	SELECT FOUND_ROWS() INTO exist_periodo_area ;
	IF exist_periodo_area!=1 THEN 
		INSERT INTO ventas (periodo_vta, monto_vta, id_arp) VALUES (NEW.periodo_vtacont, NEW.monto_vtacont, NEW.id_arp);
	ELSE 
		UPDATE ventas SET monto_vta=monto_vta+NEW.monto_vtacont WHERE id_vta=lc_id_vta;
	END IF;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `update_sum_ventacont`;
DELIMITER $$
CREATE TRIGGER `update_sum_ventacont` AFTER UPDATE ON `contado` FOR EACH ROW BEGIN
	DECLARE exist_periodo_area INT DEFAULT 0;
	DECLARE lc_id_vta INT DEFAULT NULL;
	SELECT id_vta INTO lc_id_vta FROM ventas WHERE id_arp=NEW.id_arp AND DATE_FORMAT( periodo_vta, '%m-%Y') = DATE_FORMAT( NEW.periodo_vtacont, '%m-%Y');
	SELECT FOUND_ROWS() INTO exist_periodo_area ;
	IF exist_periodo_area=1 THEN 
		UPDATE ventas SET monto_vta=(monto_vta-OLD.monto_vtacont)+NEW.monto_vtacont WHERE id_vta=lc_id_vta;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `costo_ventas`
--

DROP TABLE IF EXISTS `costo_ventas`;
CREATE TABLE `costo_ventas` (
  `id_cv` int(11) NOT NULL,
  `periodo_cv` date DEFAULT NULL,
  `monto_cv` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `costo_ventas`
--

INSERT IGNORE INTO `costo_ventas` (`id_cv`, `periodo_cv`, `monto_cv`, `id_arp`) VALUES
(1, '2018-03-06', '1000.00', 7),
(2, '2018-03-06', '1870.00', 7),
(3, '2018-03-06', '1900.00', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `credito`
--

DROP TABLE IF EXISTS `credito`;
CREATE TABLE `credito` (
  `id_vtacred` int(11) NOT NULL,
  `periodo_vtacred` date DEFAULT NULL,
  `monto_vtacred` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `credito`
--

INSERT IGNORE INTO `credito` (`id_vtacred`, `periodo_vtacred`, `monto_vtacred`, `id_arp`) VALUES
(1, '2018-10-05', '120.00', 8);

--
-- Disparadores `credito`
--
DROP TRIGGER IF EXISTS `add_sum_ventacred`;
DELIMITER $$
CREATE TRIGGER `add_sum_ventacred` AFTER INSERT ON `credito` FOR EACH ROW BEGIN
	DECLARE exist_periodo_area INT DEFAULT 0;
	DECLARE lc_id_vta INT DEFAULT NULL;
	SELECT id_vta INTO lc_id_vta FROM ventas WHERE id_arp=NEW.id_arp AND DATE_FORMAT( periodo_vta, '%m-%Y') = DATE_FORMAT( NEW.periodo_vtacred, '%m-%Y');
	SELECT FOUND_ROWS() INTO exist_periodo_area ;
	IF exist_periodo_area!=1 THEN 
		INSERT INTO ventas (periodo_vta, monto_vta, id_arp) VALUES (NEW.periodo_vtacred, NEW.monto_vtacred, NEW.id_arp);
	ELSE 
		UPDATE ventas SET monto_vta=monto_vta+NEW.monto_vtacred WHERE id_vta=lc_id_vta;
	END IF;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `update_sum_ventacred`;
DELIMITER $$
CREATE TRIGGER `update_sum_ventacred` AFTER UPDATE ON `credito` FOR EACH ROW BEGIN
	DECLARE exist_periodo_area INT DEFAULT 0;
	DECLARE lc_id_vta INT DEFAULT NULL;
	SELECT id_vta INTO lc_id_vta FROM ventas WHERE id_arp=NEW.id_arp AND DATE_FORMAT( periodo_vta, '%m-%Y') = DATE_FORMAT( NEW.periodo_vtacred, '%m-%Y');
	SELECT FOUND_ROWS() INTO exist_periodo_area ;
	IF exist_periodo_area=1 THEN 
		UPDATE ventas SET monto_vta=(monto_vta-OLD.monto_vtacred)+NEW.monto_vtacred WHERE id_vta=lc_id_vta;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_resultado`
--

DROP TABLE IF EXISTS `estado_resultado`;
CREATE TABLE `estado_resultado` (
  `id_er` int(11) NOT NULL,
  `id_vta` int(11) DEFAULT NULL,
  `monto_vta` decimal(10,2) DEFAULT NULL,
  `id_cv` int(11) DEFAULT NULL,
  `monto_cv` decimal(10,2) DEFAULT NULL,
  `utilbruta_er` decimal(10,2) DEFAULT NULL,
  `id_gg` int(11) DEFAULT NULL,
  `monto_gg` decimal(10,2) DEFAULT NULL,
  `utiloperativ_er` decimal(10,2) DEFAULT NULL,
  `impuestos_er` decimal(10,2) DEFAULT NULL,
  `utilneta_er` decimal(10,2) DEFAULT NULL,
  `periodo_er` date DEFAULT NULL,
  `saldoinicial_er` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos_generales`
--

DROP TABLE IF EXISTS `gastos_generales`;
CREATE TABLE `gastos_generales` (
  `id_gg` int(11) NOT NULL,
  `id_arp` int(11) NOT NULL,
  `id_sue` int(11) DEFAULT NULL,
  `monto_sue` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_inu` int(11) DEFAULT NULL,
  `monto_inu` decimal(10,2) DEFAULT '0.00',
  `id_srv` int(11) DEFAULT NULL,
  `monto_srv` decimal(10,2) DEFAULT '0.00',
  `id_alq` int(11) DEFAULT NULL,
  `monto_alq` decimal(10,2) DEFAULT '0.00',
  `id_otri` int(11) DEFAULT NULL,
  `monto_otri` decimal(10,2) DEFAULT '0.00',
  `id_otrg` int(11) DEFAULT NULL,
  `monto_otrg` decimal(10,2) DEFAULT '0.00',
  `periodo_gg` date DEFAULT NULL,
  `monto_gg` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto_ventas`
--

DROP TABLE IF EXISTS `gasto_ventas`;
CREATE TABLE `gasto_ventas` (
  `id_gv` int(11) NOT NULL,
  `periodo_gv` date DEFAULT NULL,
  `monto_gv` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuestos`
--

DROP TABLE IF EXISTS `impuestos`;
CREATE TABLE `impuestos` (
  `id_imp` int(11) NOT NULL,
  `factor_imp` decimal(10,2) DEFAULT NULL,
  `activo_imp` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insumos_utiles`
--

DROP TABLE IF EXISTS `insumos_utiles`;
CREATE TABLE `insumos_utiles` (
  `id_inu` int(11) NOT NULL,
  `periodo_inu` date DEFAULT NULL,
  `monto_inu` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Disparadores `insumos_utiles`
--
DROP TRIGGER IF EXISTS `add_mto_insum_utiles`;
DELIMITER $$
CREATE TRIGGER `add_mto_insum_utiles` AFTER INSERT ON `insumos_utiles` FOR EACH ROW BEGIN
	DECLARE exist_periodo_area INT DEFAULT 0;
	DECLARE lc_id_gg INT DEFAULT NULL;
	SELECT id_gg INTO lc_id_gg FROM gastos_generales WHERE id_arp=NEW.id_arp AND DATE_FORMAT( periodo_gg, '%m-%Y') = DATE_FORMAT( NEW.periodo_inu, '%m-%Y');
	SELECT FOUND_ROWS() INTO exist_periodo_area ;
	IF exist_periodo_area!=1 THEN 
		INSERT INTO gastos_generales (periodo_gg, id_inu, monto_inu, id_arp, monto_gg) VALUES (NEW.periodo_inu, NEW.id_inu, NEW.monto_inu, NEW.id_arp, NEW.monto_inu);
	ELSE 
		UPDATE gastos_generales SET id_inu=NEW.id_inu, monto_inu=NEW.monto_inu, monto_gg=monto_gg+NEW.monto_inu WHERE id_vta=lc_id_vta;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventarios`
--

DROP TABLE IF EXISTS `inventarios`;
CREATE TABLE `inventarios` (
  `id_inv` int(11) NOT NULL,
  `periodo_ii_inv` date DEFAULT NULL,
  `monto_ii_inv` decimal(10,2) DEFAULT NULL,
  `periodo_if_inv` date DEFAULT NULL,
  `monto_if_inv` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `otros_gastos`
--

DROP TABLE IF EXISTS `otros_gastos`;
CREATE TABLE `otros_gastos` (
  `id_otrg` int(11) NOT NULL,
  `periodo_otrg` date DEFAULT NULL,
  `monto_otrg` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Disparadores `otros_gastos`
--
DROP TRIGGER IF EXISTS `add_mto_ogastos`;
DELIMITER $$
CREATE TRIGGER `add_mto_ogastos` AFTER INSERT ON `otros_gastos` FOR EACH ROW BEGIN
	DECLARE exist_periodo_area INT DEFAULT 0;
	DECLARE lc_id_gg INT DEFAULT NULL;
	SELECT id_gg INTO lc_id_gg FROM gastos_generales WHERE id_arp=NEW.id_arp AND DATE_FORMAT( periodo_gg, '%m-%Y') = DATE_FORMAT( NEW.periodo_otrg, '%m-%Y');
	SELECT FOUND_ROWS() INTO exist_periodo_area ;
	IF exist_periodo_area!=1 THEN 
		INSERT INTO gastos_generales (periodo_gg, id_otrg, monto_otrg, id_arp, monto_gg) VALUES (NEW.periodo_otrg, NEW.id_otrg, NEW.monto_otrg, NEW.id_arp, NEW.monto_otrg);
	ELSE 
		UPDATE gastos_generales SET id_otrg=NEW.id_otrg, monto_otrg=NEW.monto_otrg, monto_gg=monto_gg+NEW.monto_otrg WHERE id_vta=lc_id_vta;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `otros_impuestos`
--

DROP TABLE IF EXISTS `otros_impuestos`;
CREATE TABLE `otros_impuestos` (
  `id_otri` int(11) NOT NULL,
  `periodo_otri` date DEFAULT NULL,
  `monto_otri` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Disparadores `otros_impuestos`
--
DROP TRIGGER IF EXISTS `add_mto_oimpuestos`;
DELIMITER $$
CREATE TRIGGER `add_mto_oimpuestos` AFTER INSERT ON `otros_impuestos` FOR EACH ROW BEGIN
	DECLARE exist_periodo_area INT DEFAULT 0;
	DECLARE lc_id_gg INT DEFAULT NULL;
	SELECT id_gg INTO lc_id_gg FROM gastos_generales WHERE id_arp=NEW.id_arp AND DATE_FORMAT( periodo_gg, '%m-%Y') = DATE_FORMAT( NEW.periodo_otri, '%m-%Y');
	SELECT FOUND_ROWS() INTO exist_periodo_area ;
	IF exist_periodo_area!=1 THEN 
		INSERT INTO gastos_generales (periodo_gg, id_otri, monto_otri, id_arp, monto_gg) VALUES (NEW.periodo_otri, NEW.id_otri, NEW.monto_otri, NEW.id_arp, NEW.monto_otri);
	ELSE 
		UPDATE gastos_generales SET id_otri=NEW.id_otri, monto_otri=NEW.monto_otri, monto_gg=monto_gg+NEW.monto_otri WHERE id_vta=lc_id_vta;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

DROP TABLE IF EXISTS `servicios`;
CREATE TABLE `servicios` (
  `id_srv` int(11) NOT NULL,
  `periodo_srv` date DEFAULT NULL,
  `monto_srv` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Disparadores `servicios`
--
DROP TRIGGER IF EXISTS `add_mto_servicios`;
DELIMITER $$
CREATE TRIGGER `add_mto_servicios` AFTER INSERT ON `servicios` FOR EACH ROW BEGIN
	DECLARE exist_periodo_area INT DEFAULT 0;
	DECLARE lc_id_gg INT DEFAULT NULL;
	SELECT id_gg INTO lc_id_gg FROM gastos_generales WHERE id_arp=NEW.id_arp AND DATE_FORMAT( periodo_gg, '%m-%Y') = DATE_FORMAT( NEW.periodo_srv, '%m-%Y');
	SELECT FOUND_ROWS() INTO exist_periodo_area ;
	IF exist_periodo_area!=1 THEN 
		INSERT INTO gastos_generales (periodo_gg, id_srv, monto_srv, id_arp, monto_gg) VALUES (NEW.periodo_srv, NEW.id_srv, NEW.monto_srv, NEW.id_arp, NEW.monto_srv);
	ELSE 
		UPDATE gastos_generales SET id_srv=NEW.id_srv, monto_srv=NEW.monto_srv, monto_gg=monto_gg+NEW.monto_srv WHERE id_vta=lc_id_vta;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sueldos`
--

DROP TABLE IF EXISTS `sueldos`;
CREATE TABLE `sueldos` (
  `id_sue` int(11) NOT NULL,
  `periodo_sue` date DEFAULT NULL,
  `monto_sue` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Disparadores `sueldos`
--
DROP TRIGGER IF EXISTS `add_mto_sueldo`;
DELIMITER $$
CREATE TRIGGER `add_mto_sueldo` AFTER INSERT ON `sueldos` FOR EACH ROW BEGIN
	DECLARE exist_periodo_area INT DEFAULT 0;
	DECLARE lc_id_gg INT DEFAULT NULL;
	SELECT id_gg INTO lc_id_gg FROM gastos_generales WHERE id_arp=NEW.id_arp AND DATE_FORMAT( periodo_gg, '%m-%Y') = DATE_FORMAT( NEW.periodo_sue, '%m-%Y');
	SELECT FOUND_ROWS() INTO exist_periodo_area ;
	IF exist_periodo_area!=1 THEN 
		INSERT INTO gastos_generales (periodo_gg, id_sue, monto_sue, id_arp, monto_gg) VALUES (NEW.periodo_sue, NEW.id_sue, NEW.monto_sue, NEW.id_arp, NEW.monto_sue);
	ELSE 
		UPDATE gastos_generales SET id_sue=NEW.id_sue, monto_sue=NEW.monto_sue, monto_gg=monto_gg+NEW.monto_sue WHERE id_vta=lc_id_vta;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id_usr` int(11) NOT NULL,
  `nomb_usr` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `clave_usr` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `rol_usr` enum('1','2','3') COLLATE utf8_unicode_ci NOT NULL DEFAULT '3',
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT IGNORE INTO `usuario` (`id_usr`, `nomb_usr`, `clave_usr`, `rol_usr`, `status`) VALUES
(1, 'usuario', '833693d43e103bda03c45024d6472dc1', '1', '1'),
(2, 'root', 'f64ba71a5d1ccd651a3d742b1e9c5c77', '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

DROP TABLE IF EXISTS `ventas`;
CREATE TABLE `ventas` (
  `id_vta` int(11) NOT NULL,
  `periodo_vta` date DEFAULT NULL,
  `monto_vta` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alquiler`
--
ALTER TABLE `alquiler`
  ADD PRIMARY KEY (`id_alq`);

--
-- Indices de la tabla `area_proyecto`
--
ALTER TABLE `area_proyecto`
  ADD PRIMARY KEY (`id_arp`);

--
-- Indices de la tabla `contado`
--
ALTER TABLE `contado`
  ADD PRIMARY KEY (`id_vtacont`);

--
-- Indices de la tabla `costo_ventas`
--
ALTER TABLE `costo_ventas`
  ADD PRIMARY KEY (`id_cv`);

--
-- Indices de la tabla `credito`
--
ALTER TABLE `credito`
  ADD PRIMARY KEY (`id_vtacred`),
  ADD KEY `id_arp_fk` (`id_arp`);

--
-- Indices de la tabla `estado_resultado`
--
ALTER TABLE `estado_resultado`
  ADD PRIMARY KEY (`id_er`);

--
-- Indices de la tabla `gastos_generales`
--
ALTER TABLE `gastos_generales`
  ADD PRIMARY KEY (`id_gg`);

--
-- Indices de la tabla `gasto_ventas`
--
ALTER TABLE `gasto_ventas`
  ADD PRIMARY KEY (`id_gv`);

--
-- Indices de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  ADD PRIMARY KEY (`id_imp`);

--
-- Indices de la tabla `insumos_utiles`
--
ALTER TABLE `insumos_utiles`
  ADD PRIMARY KEY (`id_inu`);

--
-- Indices de la tabla `inventarios`
--
ALTER TABLE `inventarios`
  ADD PRIMARY KEY (`id_inv`);

--
-- Indices de la tabla `otros_gastos`
--
ALTER TABLE `otros_gastos`
  ADD PRIMARY KEY (`id_otrg`);

--
-- Indices de la tabla `otros_impuestos`
--
ALTER TABLE `otros_impuestos`
  ADD PRIMARY KEY (`id_otri`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id_srv`);

--
-- Indices de la tabla `sueldos`
--
ALTER TABLE `sueldos`
  ADD PRIMARY KEY (`id_sue`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usr`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id_vta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alquiler`
--
ALTER TABLE `alquiler`
  MODIFY `id_alq` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `area_proyecto`
--
ALTER TABLE `area_proyecto`
  MODIFY `id_arp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `contado`
--
ALTER TABLE `contado`
  MODIFY `id_vtacont` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `costo_ventas`
--
ALTER TABLE `costo_ventas`
  MODIFY `id_cv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `credito`
--
ALTER TABLE `credito`
  MODIFY `id_vtacred` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `estado_resultado`
--
ALTER TABLE `estado_resultado`
  MODIFY `id_er` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `gastos_generales`
--
ALTER TABLE `gastos_generales`
  MODIFY `id_gg` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `gasto_ventas`
--
ALTER TABLE `gasto_ventas`
  MODIFY `id_gv` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  MODIFY `id_imp` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `insumos_utiles`
--
ALTER TABLE `insumos_utiles`
  MODIFY `id_inu` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inventarios`
--
ALTER TABLE `inventarios`
  MODIFY `id_inv` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `otros_gastos`
--
ALTER TABLE `otros_gastos`
  MODIFY `id_otrg` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `otros_impuestos`
--
ALTER TABLE `otros_impuestos`
  MODIFY `id_otri` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id_srv` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sueldos`
--
ALTER TABLE `sueldos`
  MODIFY `id_sue` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id_vta` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `credito`
--
ALTER TABLE `credito`
  ADD CONSTRAINT `id_arp_fk` FOREIGN KEY (`id_arp`) REFERENCES `area_proyecto` (`id_arp`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
