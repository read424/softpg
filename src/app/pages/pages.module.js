/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages', [
    'ui.router',
    'BlurAdmin.signin',
    'BlurAdmin.sides',
    'BlurAdmin.home',
    'BlurAdmin.pages.catalogo',
    'BlurAdmin.pages.ventas',
    'BlurAdmin.pages.gastos_generales',
    'BlurAdmin.pages.costo_de_venta',
    'BlurAdmin.pages.reportes',

   
  ])
  .factory('serviceAPI', [ '$http', 'apiEndpoint', function($http, apiEndpoint){
    return {
      signIn:function(data){ return $http.post(apiEndpoint+'signin.php', data); },
      getUsuarios:function(data){ return $http.post(apiEndpoint+'usuario.php?oper=listar', data);},
      existUsuario:function(params){ return $http.post(apiEndpoint+'usuario.php?oper=existelogin', params);},
      guardarUsuario:function(data){ return $http.post(apiEndpoint+'usuario.php?oper=guardar', data);},
      statusUsuario:function(data){ return $http.post(apiEndpoint+'usuario.php?oper=statuschange', data);},
      consultarUsuario:function(data){ return $http.post(apiEndpoint+'usuario.php?oper=consultar', data);},
      getAreaproyectos:function(data){return $http.post(apiEndpoint+'areaproyecto.php?oper=listar', data)},
      existAreaproy:function(data){ return $http.post(apiEndpoint+'areaproyecto.php?oper=existareaproye', data); },
      saveAreaproyecto:function(data){ return $http.post(apiEndpoint+'areaproyecto.php?oper=guardar', data); },
      getAreaproyecto:function(data){ return $http.post(apiEndpoint+'areaproyecto.php?oper=consultar', data); },
      eliminarAreaproyecto:function(data){ return $http.post(apiEndpoint+'areaproyecto.php?oper=eliminar',data); },
      getVentasCreditos:function(data){ return $http.post(apiEndpoint+'ventacredito.php?oper=listar',data); },
      saveVentaCredito:function(data){ return $http.post(apiEndpoint+'ventacredito.php?oper=guardar', data);},
      getVentaCredito:function(data){ return $http.post(apiEndpoint+'ventacredito.php?oper=consultar', data); },
      eliminarVentaCredito:function(data){ return $http.post(apiEndpoint+'ventacredito.php?oper=eliminar',data); },
      getGastoVentas:function(data){return $http.post(apiEndpoint+'gasto_venta.php?oper=listar', data)},
      saveGastoVenta:function(data){ return $http.post(apiEndpoint+'gasto_venta.php?oper=guardar', data); },
      getGastoVenta:function(data){ return $http.post(apiEndpoint+'gasto_venta.php?oper=consultar', data); },
      eliminarGastoVenta:function(data){ return $http.post(apiEndpoint+'gasto_venta.php?oper=eliminar',data); },
      getAlquileres:function(data){return $http.post(apiEndpoint+'alquiler.php?oper=listar', data)},
      savealquiler:function(data){ return $http.post(apiEndpoint+'alquiler.php?oper=guardar', data); },
      getAlquiler:function(params){ return $http.post(apiEndpoint+'alquiler.php?oper=consultar', params); },
      eliminarAlquiler:function(data){ return $http.post(apiEndpoint+'alquiler.php?oper=eliminar',data); },
      getInsumos:function(data){return $http.post(apiEndpoint+'insumo.php?oper=listar', data)},
      saveInsumo:function(data){ return $http.post(apiEndpoint+'insumo.php?oper=guardar', data); },
      getInsumo:function(data){ return $http.post(apiEndpoint+'insumo.php?oper=consultar', data); },
      eliminarInsumo:function(data){ return $http.post(apiEndpoint+'insumo.php?oper=eliminar',data); },
      getOtrosgastos:function(data){return $http.post(apiEndpoint+'otrosgastos.php?oper=listar', data)},
      saveGastos:function(data){ return $http.post(apiEndpoint+'otrosgastos.php?oper=guardar', data); },
      getOtrogasto:function(data){ return $http.post(apiEndpoint+'otrosgastos.php?oper=consultar', data); },
      eliminarGasto:function(data){ return $http.post(apiEndpoint+'otrosgastos.php?oper=eliminar',data); },
      getOtrosImpuestos:function(data){return $http.post(apiEndpoint+'otrosimpuestos.php?oper=listar', data)},
      saveImpuestos:function(data){ return $http.post(apiEndpoint+'otrosimpuestos.php?oper=guardar', data); },
      getOtroimpuesto:function(data){ return $http.post(apiEndpoint+'otrosimpuestos.php?oper=consultar', data); },
      eliminarImpuestoimpuesto:function(data){ return $http.post(apiEndpoint+'otrosimpuestos.php?oper=eliminar',data); },
      getSueldos:function(data){return $http.post(apiEndpoint+'sueldo.php?oper=listar', data)},
      saveSueldos:function(data){ return $http.post(apiEndpoint+'sueldo.php?oper=guardar', data); },
      getSueldo:function(data){ return $http.post(apiEndpoint+'sueldo.php?oper=consultar', data); },
      eliminarsueldo:function(data){ return $http.post(apiEndpoint+'sueldo.php?oper=eliminar',data); },
      getconsultarventacontado:function(data){ return $http.post(apiEndpoint+'ventacontado.php?oper=consultar', data);},
      saveguardarventacontado:function(data){ return $http.post(apiEndpoint+'ventacontado.php?oper=guardar', data); },
      getventacontado:function(data){return $http.post(apiEndpoint+'ventacontado.php?oper=listar', data)},
      eliminarventacontado:function(data){ return $http.post(apiEndpoint+'ventacontado.php?oper=eliminar',data); },
      saveInventario:function(data){ return $http.post(apiEndpoint+'inventario.php?oper=guardar', data); },
      getInventarios:function(data){ return $http.post(apiEndpoint+'inventario.php?oper=listar', data); },
      getInventario:function(data){ return $http.post(apiEndpoint+'inventario.php?oper=consultar', data); },    
      getservicios:function(data){ return $http.post(apiEndpoint+'servicio.php?oper=consultar', data);},
      saveServivio:function(data){ return $http.post(apiEndpoint+'servicio.php?oper=guardar', data); },
      getServicio:function(data){return $http.post(apiEndpoint+'servicio.php?oper=listar', data)},
      eliminarServicio:function(data){ return $http.post(apiEndpoint+'servicio.php?oper=eliminar',data); },
      getImpuestos:function(data){return $http.post(apiEndpoint+'impuestos.php?oper=listar', data)},
      saveImpuesto:function(data){ return $http.post(apiEndpoint+'impuestos.php?oper=guardar', data); },
      getImpuesto:function(data){ return $http.post(apiEndpoint+'impuestos.php?oper=consultar', data); },
      eliminarImpuesto:function(data){ return $http.post(apiEndpoint+'impuestos.php?oper=eliminar',data); },
      changeDefaultImp:function(params){ return $http.post(apiEndpoint+'impuestos.php?oper=changedefault', params); },
      existResultPeriodo:function(params){ return $http.post(apiEndpoint+'reporte.php?oper=existresultado', params);},
      getResultPeriodo:function(params){ return $http.post(apiEndpoint+'reporte.php?oper=getresultado', params); }
    }
    }]);

})();
