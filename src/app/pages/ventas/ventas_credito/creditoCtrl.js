(function(){
    'use strict';
    angular.module('BlurAdmin.pages.ventas.credito',[])
        .controller('creditoCtrl', [ '$scope', '$state', 'serviceAPI', '$stateParams', 'toastr', function($scope, $state, serviceAPI, $stateParams, toastr){
            $scope.default={id_vtacredito:'', id_area:'', periodo:'', monto:0.00};
            $scope.data={id_vtacredito:'', id_area:'', periodo:'', monto:0.00};
            $scope.areas_proyectos=[];
            serviceAPI.getAreaproyectos().success(function(r){
                if(r.success && r.totalItemCount>0){
                    $scope.areas_proyectos=r.rows.map(function(i){ return {id: i.id_arp ,label: i.desc_arp};})

                }
            });
            $scope.listado=function(){
                $state.go('dashboard.ventas.credito');
            };
            $scope.consultar=function(){
                if($stateParams.idcredito=='')
                    return false;
                serviceAPI.getVentaCredito({id:$stateParams.idcredito}).success(function(r){
                    if(r.success && r.num_rows==1){
                        r.rows.periodo=new Date(r.rows.periodo);
                        $scope.areas_proyectos.filter(function(i){ 
                            if(i.id==r.rows.id_area){
                                r.rows.id_area=i;
                            }
                        });
                        angular.extend($scope.data, r.rows);
                    }else{
                        toastr.error(r.messages, 'SOFTPG')
                    }
                });
            };
            $scope.guardar=function(){
                serviceAPI.saveVentaCredito($scope.data).success(function(response){
                    var typeNotify=(response.success)?'warning':'error';
                    if(response.success && response.affected_rows==1){
                        typeNotify='success';
                    }
                    angular.extend($scope.data, $scope.default);
                    toastr[typeNotify](response.messages, 'SOFTPG');
                });
            };
        }])
        .controller('creditoListCtrl', ['$scope', '$state', 'serviceAPI', function($scope, $state, serviceAPI){
            $scope.smartTablePageSize = 10;
            $scope.displayed=[];
            $scope.callServer=function(tableState){
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getVentasCreditos(params).success(function(response){
                    if(response.success && response.totalItemCount>0){
                        $scope.displayed=response.rows;
                        tableState.pagination.numberOfPages =response.numberOfPages;
                    }    
                });
            };
            $scope.agregar=function(){
                $state.go('dashboard.ventas.credito-detail');
            };
            $scope.editar=function(id){
                $state.go('dashboard.ventas.credito-detail', {idcredito:id});
            };
            $scope.eliminar=function(id, position){
                serviceAPI.eliminarVentaCredito({id:id}).success(function(response){
                    if(response.success && response.affected_rows){
                        $scope.displayed=$scope.displayed.filter(function(i){ return (i.id_arp!=id); });
                    }else
                        toastr.error(response.messages, 'SOFTPG');
                });
            };
        }]);
;
} )()
