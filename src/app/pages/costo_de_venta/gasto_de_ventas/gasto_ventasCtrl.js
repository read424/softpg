(function(){
    'use strict';
    angular.module('BlurAdmin.pages.costo_de_venta.gasto_de_ventas',[])
        .controller('gasto_ventasCtrl', [ '$scope', '$state', '$stateParams', 'serviceAPI', 'toastr', function($scope, $state, $stateParams, serviceAPI, toastr){
            $scope.default={id_gv:'', id_area:'', periodo:'', monto:0.00};
            $scope.data={id_gv:'', id_area:'', periodo:'', monto:0.00};
            $scope.areas_proyectos=[];
            var areas_proyectos=[];
            serviceAPI.getAreaproyectos().success(function(r){
                if(r.success && r.rows.length>0){
                    areas_proyectos=r.rows.map(function(i){ return {id: i.id_arp ,label: i.desc_arp};});
                    angular.copy(areas_proyectos, $scope.areas_proyectos);
                }
            });

            $scope.listado=function(){
                $state.go('dashboard.costo_de_venta.gasto_de_ventas');
            };

            $scope.consultar=function(){
                if($stateParams.idgastoventa=='' || angular.isUndefined($stateParams.idgastoventa))
                    return false;
                serviceAPI.getGastoVenta({id:$stateParams.idgastoventa}).then(function(response){
                    if(response.data.success && response.data.num_rows==1){
                        response.data.rows.periodo=new Date(response.data.rows.periodo);
                        angular.extend($scope.data, response.data.rows);
                        console.log(areas_proyectos);
                        $scope.areas_proyectos.filter(function(items){
                            console.log(items.id, response.data.rows.id_area);
                            if(items.id==response.data.rows.id_area){ console.log(items); $scope.data.id_area=items; }
                        });
                    }else{
                        toastr.error(r.messages, 'SOFTPG')
                    }
                });
            };
            $scope.guardar=function(){
                serviceAPI.saveGastoVenta($scope.data).success(function(response){
                    var typeNotify=(response.success)?'warning':'error';
                    if(response.success && response.affected_rows==1){
                        typeNotify='success';
                    }
                    angular.extend($scope.data, $scope.default);
                    toastr[typeNotify](response.messages, 'SOFTPG');
                });
            };
        }])
        .controller('gasto_ventasListCtrl', ['$scope', '$state', 'serviceAPI', function($scope, $state, serviceAPI){
            $scope.smartTablePageSize = 10;
            $scope.displayed=[];
            $scope.callServer=function(tableState){
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getGastoVentas(params).success(function(response){
                    if(response.success && response.totalItemCount>0){
                        $scope.displayed=response.rows;
                        tableState.pagination.numberOfPages =response.numberOfPages;
                    }    
                });
            };
            $scope.agregar=function(){
                $state.go('dashboard.costo_de_venta.gastoventa-detail');
            };
            $scope.editar=function(id){
                $state.go('dashboard.costo_de_venta.gastoventa-detail', {idgastoventa:id});
            };
            $scope.eliminar=function(id, position){
                serviceAPI.eliminarGastoVenta({id:id}).success(function(response){
                    if(response.success && response.affected_rows){
                        $scope.displayed=$scope.displayed.filter(function(i){ return (i.id_arp!=id); });
                    }else
                        toastr.error(response.messages, 'SOFTPG');
                });
            };
        }]);
})();