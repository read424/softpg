'use strict';

angular.module('BlurAdmin', [
  'ngAnimate',
  'ngSanitize',
  'ui.bootstrap',
  'ui.sortable',
  'ui.router',
  'ngStorage',
  'permission',
  'permission.ui',
  'ngTouch',
  'toastr',
  'smart-table',
  'ui.slimscroll',
  'ngJsTree',
  'ui.select',
  'angular-progress-button-styles',
  'BlurAdmin.theme',
  'BlurAdmin.pages',
  'angular-loading-bar',
  'BlurAdmin.authService',
  'BlurAdmin.commonservice'
])
.constant('apiEndpoint', '/api/')
.config(['cfpLoadingBarProvider', '$urlRouterProvider', '$httpProvider', function(cfpLoadingBarProvider, $urlRouterProvider, $httpProvider){
  cfpLoadingBarProvider.includeSpinner = false;
  $urlRouterProvider.otherwise('/signin');
    $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function($q, $location, $localStorage){
      return {
          'request': function(config){
              config.headers=config.headers || {};
              if($localStorage.token){
                  config.headers.Authorization='Bearer '+$localStorage.token;
              }
              return config;
          },
          'response':function(response){
              if(response.data.token!==undefined){
                  $localStorage.token=response.data.token;}
              return response;
          },
          'responseError': function(response){
              if(response.status===401 || response.status==403){
                  $location.path('/sigin');
              }
              return $q.reject(response);
          }
      }
    }]);
}]);