(function(){
    'use strict';
    angular.module('BlurAdmin.pages.catalogo.impuestos', [])
        .controller('impuestosCtrl', [ '$scope', '$state', '$stateParams', 'toastr', 'serviceAPI', function($scope, $state, $stateParams, toastr, serviceAPI){
            $scope.default={id_imp:'', factor_imp:0.00, status:1};
            $scope.data={id_imp:'', factor_imp:0.00, status:1};
            $scope.consultar=function(){
                if($stateParams.idimpuesto=='' || angular.isUndefined($stateParams.idimpuesto))
                    return true;
                serviceAPI.getImpuesto({id:$stateParams.idimpuesto}).success(function(response){
                    var typeNotify=(response.success)?'warning':'error';
                    if(response.success && response.num_rows==1){
                        typeNotify='';
                        angular.extend($scope.data, response.rows);
                        $scope.data.rep_password=response.rows.password;
                    }
                    if(typeNotify!='')
                        toastr[typeNotify](response.messages, 'SOFTPG');
                });
            };
            $scope.guardar=function(){
                serviceAPI.saveImpuesto($scope.data).success(function(response){
                    var typeNotify=(response.success)?'warning':'error';
                    if(response.success && response.affected_rows==1){
                        typeNotify='success';
                    }
                    toastr[typeNotify](response.messages, 'SISPG');
                });
            };

            $scope.listado=function(){
                $state.go('dashboard.catalogo.impuestos');
            };

        }])
        .controller('impuestosListCtrl', ['$scope', '$state', 'toastr', 'serviceAPI',  function($scope, $state, toastr, serviceAPI){
            $scope.smartTablePageSize = 10;
            $scope.displayed=[];
            $scope.callServer=function(tableState){
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getImpuestos(params).success(function(response){
                    if(response.success && response.totalItemCount>0){
                        $scope.displayed=response.rows;
                        tableState.pagination.numberOfPages =response.numberOfPages;
                    }    
                });
            };
            $scope.change_default=function(id, actividad, index){
                serviceAPI.changeDefaultImp({id:id, status:actividad}).success(function(r){
                    var typenotify='error';
                    if(r.num_rows==0){
                        typenotify='warning';
                        $scope.displayed=$scope.displayed.filter(function(i){ return (i.idcuentasbancarias!=idnumcta);})
                    }
                    if(r.affected_rows==1){
                        angular.forEach($scope.displayed, function(item, i){
                            item.activo_imp=(index==i)?'1':'0';
                        });
                        typenotify='success';
                    }
                    toastr[typenotify](r.messages, 'SOFTPG');
                });
            };
            $scope.agregar=function(){
                $state.go('dashboard.catalogo.impuesto-detail');
            };
            $scope.editar=function(id){
                $state.go('dashboard.catalogo.impuesto-detail', {idimpuesto:id});
            };
            $scope.eliminar=function(id, position){
                serviceAPI.eliminarImpuesto({id:id}).success(function(response){
                    if(response.success && response.affected_rows){
                        $scope.displayed=$scope.displayed.filter(function(i){ return (i.id_arp!=id); });
                    }else
                        toastr.error(response.messages, 'SOFTPG');
                });
            };
        }])
})();