<?php
class GVentacontado extends GVentas{

    public function __construct(){
        parent::__construct();
    }

    public function consultar(){
        return "SELECT DATE_FORMAT(c.periodo_vtacont, '%d/%m/%Y') AS periodo, c.id_arp, c.monto_vtacont AS monto, c.id_vtacont FROM contado AS c WHERE id_vtacont=? ";
    }

    public function actualizar(){
        return "UPDATE contado SET periodo_vtacont=?, id_arp=?, monto_vtacont=? WHERE id_vtacont=?";
    }

    public function agregar(){
        return "INSERT INTO contado (periodo_vtacont, id_arp, monto_vtacont, id_vtacont) VALUES (?, ?, ?, ?)";
    }

    public function listar(){
        return "SELECT c.id_vtacont, DATE_FORMAT(c.periodo_vtacont, '%c') AS mes, DATE_FORMAT(c.periodo_vtacont, '%Y') AS anio, c.monto_vtacont AS monto, ap.desc_arp FROM contado AS c LEFT OUTER JOIN area_proyecto AS ap ON ap.id_arp=c.id_arp";
    }
}
?>