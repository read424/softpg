(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.gastos_generales', [
      'BlurAdmin.pages.gastos_generales.servicio',
      'BlurAdmin.pages.gastos_generales.otros_impuestos',
      'BlurAdmin.pages.gastos_generales.otros_gastos',
      'BlurAdmin.pages.gastos_generales.alquiler',
      'BlurAdmin.pages.gastos_generales.sueldos',
      'BlurAdmin.pages.gastos_generales.insumos'
    ]).config([ '$stateProvider',  function($stateProvider){
        $stateProvider.state('dashboard.gastos_generales', {
          url: '/gastos_generales',
          abstract: true,
          template : '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          title: 'Gastos Generales',
          sidebarMeta: {
            icon: 'fa fa-money',
            order: 400
          }
        }).state('dashboard.gastos_generales.sueldos', {
          url: '/sueldos',
          templateUrl: 'app/pages/gastos_generales/sueldos/list/sueldos.html',
          title: 'Sueldos',
          controller: 'sueldoListCtrl',
          sidebarMeta: {
            order: 100
          }
        }).state('dashboard.gastos_generales.sueldos-details',{
          url: '/sueldos/details/:idsueldo',
          templateUrl: 'app/pages/gastos_generales/sueldos/sueldo.html',
          title: 'Sueldos',
          controller: 'sueldoCtrl',
          params: {
            idsueldo: ''
          }
        }).state('dashboard.gastos_generales.insumos',{
          url: '/insumos',
          templateUrl: 'app/pages/gastos_generales/insumos/list/insumos.html',
          controller: 'insumoListCtrl',
          title: 'Insumos',
          sidebarMeta: {
            order: 200
          }
        }).state('dashboard.gastos_generales.insumos-detail',{
          url: '/insumos/details/:idinsumo',
          templateUrl: 'app/pages/gastos_generales/insumos/insumo.html',
          title: 'Insumos',
          controller: 'insumoCtrl',
          params: {
            idinsumo: ''
          }
        }).state('dashboard.gastos_generales.servicio', {
          url: '/servicio',
          templateUrl: 'app/pages/gastos_generales/servicios/list/servicios.html',
          title: 'Servicios',
          controller: 'servicioslistCtrl',
          sidebarMeta: {
            order: 300
          }
        }).state('dashboard.gastos_generales.servicio-detail',{
          url: '/servicio/details/:idservicio',
          templateUrl: 'app/pages/gastos_generales/servicios/servicio.html',
          controller: 'serviciosCtrl',
          title: 'Servicios',
          params: {
            idservicio: ''
          }
        }).state('dashboard.gastos_generales.alquiler', {
          url: '/alquiler',
          templateUrl: 'app/pages/gastos_generales/alquiler/list/alquileres.html',
          title: 'Alquiler',
          controller: 'alquilerlistCtrl',
          sidebarMeta: {
            order: 400
          }
        }).state('dashboard.gastos_generales.alquiler-detail',{
          url: '/alquiler/details/:idalquiler',
          templateUrl: 'app/pages/gastos_generales/alquiler/alquiler.html',
          title: 'Alquiler',
          controller: 'alquilerCtrl',
          params: {
            idalquiler: ''
          }
        }).state('dashboard.gastos_generales.otros_impuestos', {
          url: '/otros_impuestos',
          templateUrl: 'app/pages/gastos_generales/otros_impuestos/list/otros_impuestos.html',
          title: 'Impuestos',
          controller: 'otros_impuestosListCtrl',
          sidebarMeta: {
            order: 500
          }
        }).state('dashboard.gastos_generales.otroimpuesto-detail',{
          url: '/otros_impuestos/details/:idimpuesto',
          templateUrl: 'app/pages/gastos_generales/otros_impuestos/otro_impuesto.html',
          title: 'Servicios',
          controller: 'otros_impuestosCtrl',
          params: {
            idimpuesto: ''
          }
        }).state('dashboard.gastos_generales.otros_gastos', {
          url: '/otros_gastos',
          templateUrl: 'app/pages/gastos_generales/otros_gastos/list/otro_gasto.html',
          title: 'Gastos',
          controller: 'otro_gastoListCtrl',
          sidebarMeta: {
            order: 600
          }
        }).state('dashboard.gastos_generales.otrogasto-detail',{
          url: '/otros_gastos/details/:idgastos',
          templateUrl: 'app/pages/gastos_generales/otros_gastos/otros_gastos.html',
          title: 'Gastos',
          controller: 'otro_gastoCtrl',
          params: {
            idgastos: ''
          }
        });

    }]);

  })();
  