<?php
$include_config= array(
	realpath('./config.php'),
	'../api/config.php'
);
foreach ($include_config as $include_path) {
	if (@file_exists($include_path)) {
		require_once($include_path);
		break;
	}
}
class GConector extends MySQLi {
    private $status_commit;

    public function __construct(){
        parent::__construct(HOSTDB, USERDB, PASSDB, NAMEDB);
        $this->set_charset('utf-8');

    }

    public function setAutocommit($status_commit=FALSE){
		$this->status_commit=$status_commit;
		$this->autocommit($this->status_commit);
    }
    
    
    public function __destroy(){
		if($this->status_commit==FALSE)
			$this->rollback();
		$this->close();
	}    
}
?>