<?php
class GPagination{
    protected $init;
    protected $limit;
    protected $sql;
    protected $filter;

    function __construct(){
        $this->init=-1;
        $this->limit=0;
        $this->filter='';
    }
    
    function setInit($init){ $this->init=$init; }

    function getInit(){ return $this->init; }

    function setLimit($limit){ $this->limit=$limit; }

    function getLimit(){ return $this->limit; }


    function prepareSQL($sql){
        return sprintf("%s LIMIT %d, %d", $sql, $this->init, $this->limit );
    }
}
?>