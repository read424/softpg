<?php
class GOtrosgastos{

    public function __construct(){
    }

    public function consultar(){
        return "SELECT DATE_FORMAT(periodo_otrg, '%d/%m/%Y') AS periodo, id_arp, monto_otrg AS monto, id_otrg  FROM otros_gastos WHERE id_otrg=?";
    }

    public function actualizar(){
        return "UPDATE otros_gastos SET periodo_otrg=?, id_arp=?, monto_otrg=? WHERE id_otrg=?";
    }

    public function agregar(){
        return "INSERT INTO otros_gastos (periodo_otrg, id_arp, monto_otrg, id_otrg) VALUES (?, ?, ?, ?)";
    }

    public function listar(){
        return "SELECT c.id_otrg, DATE_FORMAT(c.periodo_otrg, '%c') AS mes, DATE_FORMAT(c.periodo_otrg, '%Y') AS anio, c.monto_otrg AS monto, ap.desc_arp FROM otros_gastos AS c LEFT OUTER JOIN area_proyecto AS ap ON ap.id_arp=c.id_arp";
    }
}
?>