<?php
class GAlquiler{

    public function __construct(){
    }

    public function consultar(){
        return "SELECT DATE_FORMAT(periodo_alq, '%d/%m/%Y') AS periodo, id_arp, monto_alq AS monto, id_alq  FROM alquiler WHERE id_alq=?";
    }

    public function actualizar(){
        return "UPDATE alquiler SET periodo_alq=?, id_arp=?, monto_alq=? WHERE id_alq=?";
    }

    public function agregar(){
        return "INSERT INTO alquiler (periodo_alq, id_arp, monto_alq, id_alq) VALUES (?, ?, ?, ?)";
    }

    public function listar(){
        return "SELECT c.id_alq, DATE_FORMAT(c.periodo_alq, '%c') AS mes, DATE_FORMAT(c.periodo_alq, '%Y') AS anio, c.monto_alq AS monto, ap.desc_arp FROM alquiler AS c LEFT OUTER JOIN area_proyecto AS ap ON ap.id_arp=c.id_arp";
    }
}
?>