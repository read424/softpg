<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
    $a_oper=array("existresultado", "getresultado");
    if(!isset($_GET['oper']) || !in_array($_GET['oper'], $a_oper)){
        die();
    }
    $response_json	=array('success'=>false, 'auth'=>false, 'admin'=>false, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
    $data = json_decode(file_get_contents('php://input'));
    include './class/GLibfunciones.php';
    $OConex=new GConector();
    $init_stmt=$OConex->stmt_init();
    switch($_GET['oper']){
        case 'existresultado':
            if(!isset($data->inicio, $data->fin, $data->id_area) || empty($data->inicio) || empty($data->fin) || empty($data->id_area->id))
                break;
            $fecha=DateTime::createFromFormat('Y-m-d\TH:i:s.uP', $data->inicio);
            if(!($fecha instanceof DateTime))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", "No es un formato de fecha valido", '-----');
            $fec_desde=$fecha->format('Y-m-d');
            $fecha=DateTime::createFromFormat('Y-m-d\TH:i:s.uP', $data->fin);
            if(!($fecha instanceof DateTime))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", "No es un formato de fecha valido", '-----');
            $fec_hasta=$fecha->format('Y-m-d');
            $sql="SELECT id_er FROM estado_resultado WHERE DATE_FORMAT(periodo_er, '%m-%Y') BETWEEN DATE_FORMAT(?, '%m-%Y') AND DATE_FORMAT(?, '%m-%Y') AND id_arp=?";
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('ssi', $fec_desde, $fec_hasta, $data->id_area))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $response_json['success']=true;
            $result_stmt=$init_stmt->get_result();
            $response_json['num_rows']=$result_stmt->num_rows;
            if($response_json['num_rows']<1){
                $response_json['messages']="No hay registros para la consulta";
                break;
            }
            $id_er=array();
            while($row=$result_stmt->fetch_object()){
                array_push($id_er, $row->id_er);
            }
            array_push($response_json['rows'], implode('-', $id_er));
        break;
        case 'getresultado':
            if(!isset($data->id) || empty($data->id))
                break;
            $id=explode('-', $data->id);
            $ids_string=implode(',', $id);
            $str_params=array_fill(0, count($id), '?');
            $a_parameters=array(0=>'i');
            $a_parameters[]=&$data->id_area;
            $a_parameters[0].=str_repeat('i', count($id));
            $i=1;
            foreach($id as $fields => $value){
                ${$fields}=$value; 
                $a_parameters[]=&${$fields}; 
            }
            $sql=sprintf("SELECT IFNULL(SUM(c.monto_vtacont), 0.00) AS monto_vtacont, IFNULL(SUM(cr.monto_vtacred), 0.00) AS monto_vtacred, IFNULL(SUM(inv.monto_ii_inv), 0.00) AS monto_ii, IFNULL(SUM(inv.monto_if_inv), 0.00) AS monto_if, IFNULL(SUM(gv.monto_gv), 0.00) AS monto_gv, IFNULL(SUM(er.utilbruta_er), 0.00) AS utilbruta_er, IFNULL(SUM(er.utiloperativ_er), 0.00) AS utiloperativ_er, IFNULL(SUM(er.impuestos_er), 0.00) AS impuestos_er, IFNULL(SUM(gg.monto_gg), 0.00) AS monto_gg, IFNULL(SUM(er.utilneta_er), 0.00) AS utilneta_er FROM estado_resultado AS er LEFT OUTER JOIN contado AS c ON (DATE_FORMAT(c.periodo_vtacont, '%%m-%%Y')=DATE_FORMAT(er.periodo_er, '%%m-%%Y') AND c.id_arp=er.id_arp) LEFT OUTER JOIN credito AS cr ON (DATE_FORMAT(cr.periodo_vtacred, '%%m-%%Y')=DATE_FORMAT(er.periodo_er, '%%m-%%Y') AND cr.id_arp=er.id_arp) LEFT OUTER JOIN inventarios AS inv ON (DATE_FORMAT(inv.periodo_ii_inv, '%%m-%%Y')=DATE_FORMAT(er.periodo_er, '%%m-%%Y') AND inv.id_arp=er.id_arp) LEFT OUTER JOIN gasto_ventas AS gv ON (DATE_FORMAT(gv.periodo_gv, '%%m-%%Y')=DATE_FORMAT(er.periodo_er, '%%m-%%Y') AND gv.id_arp=er.id_arp) LEFT OUTER JOIN gastos_generales AS gg ON (DATE_FORMAT(gg.periodo_gg, '%%m-%%Y')=DATE_FORMAT(er.periodo_er, '%%m-%%Y') AND gv.id_arp=er.id_arp) WHERE er.id_arp=? AND er.id_er IN (%s) GROUP BY er.id_arp, DATE_FORMAT(er.periodo_er, '%%m-%%Y')", implode(",", $str_params) );
            if(!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!call_user_func_array(array($init_stmt, 'bind_param'), $a_parameters))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $result_stmt=$init_stmt->get_result();
            $response_json['success']=true;
            $response_json['num_rows']=$result_stmt->num_rows;
            if($result_stmt->num_rows==1){
                $response_json['rows']=$result_stmt->fetch_assoc();
                $response_json['rows']['tot_vta']=$response_json['rows']['monto_vtacont']+$response_json['rows']['monto_vtacred'];
                $response_json['rows']['tot_cv']=$response_json['rows']['monto_ii']+$response_json['rows']['monto_gv']-$response_json['rows']['monto_if'];
            }else
                $response_json['messages']="No se encontraron registros que coincida con la consulta";
        break;
    }
    echo json_encode($response_json);
}catch(Exception $e){
    echo $e->getOutMsg();
}
?>