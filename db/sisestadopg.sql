-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-10-2018 a las 22:18:12
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.0.31

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sisestadopg`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alquiler`
--

CREATE TABLE `alquiler` (
  `id_alq` int(11) NOT NULL,
  `periodo_alq` date DEFAULT NULL,
  `monto_alq` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area_proyecto`
--

CREATE TABLE `area_proyecto` (
  `id_arp` int(11) NOT NULL,
  `desc_arp` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contado`
--

CREATE TABLE `contado` (
  `id_vtacont` int(11) NOT NULL,
  `periodo_vtacont` date DEFAULT NULL,
  `monto_vtacont` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `costo_ventas`
--

CREATE TABLE `costo_ventas` (
  `id_cv` int(11) NOT NULL,
  `periodo_cv` date DEFAULT NULL,
  `monto_cv` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `credito`
--

CREATE TABLE `credito` (
  `id_vtacred` int(11) NOT NULL,
  `periodo_vtacred` date DEFAULT NULL,
  `monto_vtacred` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_resultado`
--

CREATE TABLE `estado_resultado` (
  `id_er` int(11) NOT NULL,
  `id_vta` int(11) DEFAULT NULL,
  `monto_vta` decimal(10,2) DEFAULT NULL,
  `id_cv` int(11) DEFAULT NULL,
  `monto_cv` decimal(10,2) DEFAULT NULL,
  `utilbruta_er` decimal(10,2) DEFAULT NULL,
  `id_gg` int(11) DEFAULT NULL,
  `monto_gg` decimal(10,2) DEFAULT NULL,
  `utiloperativ_er` decimal(10,2) DEFAULT NULL,
  `impuestos_er` decimal(10,2) DEFAULT NULL,
  `utilneta_er` decimal(10,2) DEFAULT NULL,
  `periodo_er` date DEFAULT NULL,
  `saldoinicial_er` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos_generales`
--

CREATE TABLE `gastos_generales` (
  `id_gg` int(11) NOT NULL,
  `id_sue` int(11) DEFAULT NULL,
  `monto_sue` decimal(10,2) DEFAULT NULL,
  `id_inu` int(11) DEFAULT NULL,
  `monto_inu` decimal(10,2) DEFAULT NULL,
  `id_srv` int(11) DEFAULT NULL,
  `monto_srv` decimal(10,2) DEFAULT NULL,
  `id_alq` int(11) DEFAULT NULL,
  `monto_alq` decimal(10,2) DEFAULT NULL,
  `id_otri` int(11) DEFAULT NULL,
  `monto_otri` decimal(10,2) DEFAULT NULL,
  `id_otrg` int(11) DEFAULT NULL,
  `monto_otrg` decimal(10,2) DEFAULT NULL,
  `periodo_gg` date DEFAULT NULL,
  `monto_gg` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto_ventas`
--

CREATE TABLE `gasto_ventas` (
  `id_gv` int(11) NOT NULL,
  `periodo_gv` date DEFAULT NULL,
  `monto_gv` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuestos`
--

CREATE TABLE `impuestos` (
  `id_imp` int(11) NOT NULL,
  `factor_imp` decimal(10,2) DEFAULT NULL,
  `activo_imp` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insumos_utiles`
--

CREATE TABLE `insumos_utiles` (
  `id_inu` int(11) NOT NULL,
  `periodo_inu` date DEFAULT NULL,
  `monto_inu` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventarios`
--

CREATE TABLE `inventarios` (
  `id_inv` int(11) NOT NULL,
  `periodo_ii_inv` date DEFAULT NULL,
  `monto_ii_inv` decimal(10,2) DEFAULT NULL,
  `periodo_if_inv` date DEFAULT NULL,
  `monto_if_inv` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `otros_gastos`
--

CREATE TABLE `otros_gastos` (
  `id_otrg` int(11) NOT NULL,
  `periodo_otrg` date DEFAULT NULL,
  `monto_otrg` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `otros_impuestos`
--

CREATE TABLE `otros_impuestos` (
  `id_otri` int(11) NOT NULL,
  `periodo_otri` date DEFAULT NULL,
  `monto_otri` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `id_srv` int(11) NOT NULL,
  `periodo_srv` date DEFAULT NULL,
  `monto_srv` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sueldos`
--

CREATE TABLE `sueldos` (
  `id_sue` int(11) NOT NULL,
  `periodo_sue` date DEFAULT NULL,
  `monto_sue` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usr` int(11) NOT NULL,
  `nomb_usr` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `clave_usr` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `rol_usr` enum('1','2','3') COLLATE utf8_unicode_ci NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usr`, `nomb_usr`, `clave_usr`, `rol_usr`) VALUES
(1, 'usuario', '833693d43e103bda03c45024d6472dc1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id_vta` int(11) NOT NULL,
  `periodo_vta` date DEFAULT NULL,
  `monto_vta` decimal(10,2) DEFAULT NULL,
  `id_arp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alquiler`
--
ALTER TABLE `alquiler`
  ADD PRIMARY KEY (`id_alq`);

--
-- Indices de la tabla `area_proyecto`
--
ALTER TABLE `area_proyecto`
  ADD PRIMARY KEY (`id_arp`);

--
-- Indices de la tabla `contado`
--
ALTER TABLE `contado`
  ADD PRIMARY KEY (`id_vtacont`);

--
-- Indices de la tabla `costo_ventas`
--
ALTER TABLE `costo_ventas`
  ADD PRIMARY KEY (`id_cv`);

--
-- Indices de la tabla `credito`
--
ALTER TABLE `credito`
  ADD PRIMARY KEY (`id_vtacred`);

--
-- Indices de la tabla `estado_resultado`
--
ALTER TABLE `estado_resultado`
  ADD PRIMARY KEY (`id_er`);

--
-- Indices de la tabla `gastos_generales`
--
ALTER TABLE `gastos_generales`
  ADD PRIMARY KEY (`id_gg`);

--
-- Indices de la tabla `gasto_ventas`
--
ALTER TABLE `gasto_ventas`
  ADD PRIMARY KEY (`id_gv`);

--
-- Indices de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  ADD PRIMARY KEY (`id_imp`);

--
-- Indices de la tabla `insumos_utiles`
--
ALTER TABLE `insumos_utiles`
  ADD PRIMARY KEY (`id_inu`);

--
-- Indices de la tabla `inventarios`
--
ALTER TABLE `inventarios`
  ADD PRIMARY KEY (`id_inv`);

--
-- Indices de la tabla `otros_gastos`
--
ALTER TABLE `otros_gastos`
  ADD PRIMARY KEY (`id_otrg`);

--
-- Indices de la tabla `otros_impuestos`
--
ALTER TABLE `otros_impuestos`
  ADD PRIMARY KEY (`id_otri`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id_srv`);

--
-- Indices de la tabla `sueldos`
--
ALTER TABLE `sueldos`
  ADD PRIMARY KEY (`id_sue`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usr`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id_vta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alquiler`
--
ALTER TABLE `alquiler`
  MODIFY `id_alq` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `area_proyecto`
--
ALTER TABLE `area_proyecto`
  MODIFY `id_arp` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `contado`
--
ALTER TABLE `contado`
  MODIFY `id_vtacont` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `costo_ventas`
--
ALTER TABLE `costo_ventas`
  MODIFY `id_cv` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `credito`
--
ALTER TABLE `credito`
  MODIFY `id_vtacred` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `estado_resultado`
--
ALTER TABLE `estado_resultado`
  MODIFY `id_er` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `gastos_generales`
--
ALTER TABLE `gastos_generales`
  MODIFY `id_gg` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `gasto_ventas`
--
ALTER TABLE `gasto_ventas`
  MODIFY `id_gv` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  MODIFY `id_imp` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `insumos_utiles`
--
ALTER TABLE `insumos_utiles`
  MODIFY `id_inu` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inventarios`
--
ALTER TABLE `inventarios`
  MODIFY `id_inv` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `otros_gastos`
--
ALTER TABLE `otros_gastos`
  MODIFY `id_otrg` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `otros_impuestos`
--
ALTER TABLE `otros_impuestos`
  MODIFY `id_otri` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id_srv` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sueldos`
--
ALTER TABLE `sueldos`
  MODIFY `id_sue` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id_vta` int(11) NOT NULL AUTO_INCREMENT;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
